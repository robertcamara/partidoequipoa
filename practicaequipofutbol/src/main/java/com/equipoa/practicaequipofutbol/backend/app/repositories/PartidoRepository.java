package com.equipoa.practicaequipofutbol.backend.app.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.equipoa.practicaequipofutbol.backend.modelo.Equipo;
import com.equipoa.practicaequipofutbol.backend.modelo.Partido;

public interface PartidoRepository extends JpaRepository<Partido, Integer>{

	public List<Partido> getByJornada(String id);

	public List<Partido> getByVisitante(Integer id);
	public List<Partido> getByLocal(Integer id);
	
	@Query("select p from Partido p where p.visitante = ?1 or p.local = ?1")
	public List<Partido> getByEquipo(Equipo equipo);
	
	@Query("select p from Partido p where p.estado <> 'PENDIENTE'")
	public List<Partido> getPartidosPuntuables();
	
}
