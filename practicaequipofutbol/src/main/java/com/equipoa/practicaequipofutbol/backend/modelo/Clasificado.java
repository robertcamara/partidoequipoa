package com.equipoa.practicaequipofutbol.backend.modelo;

public class Clasificado {

	private Equipo equipo;
	
	//-----------LOCAL-------------
	
	private int PGL;//PartidosGanadosLocal
	private int PPL;//PartidosPerdidosLocal
	private int PEL;//PartidosEmpatadosLocal
	private int GFL;//GolesFavorLocal
	private int GCL;//GolesContraLocal
	
	//---------VISITANTE------------
	private int PGV;//PartidosGanadosVisitante
	private int PPV;//PartidosPerdidosVisitante
	private int PEV;//PartidosEmpatadosVisitante
	private int GFV;//GolesFavorVisitante
	private int GCV;//GolesContraVisitante
	
	public Clasificado() {
		
	}
	
	public Clasificado(Equipo equipo, int pGL, int pPL, int pEL, int gVL, int gCL, int pGV, int pPV, int pEV, int gVV,
			int gCV) {
		this.equipo = equipo;
		PGL = pGL;
		PPL = pPL;
		PEL = pEL;
		GFL = gVL;
		GCL = gCL;
		PGV = pGV;
		PPV = pPV;
		PEV = pEV;
		GFV = gVV;
		GCV = gCV;
	}


	
	public Equipo getEquipo() {
		return equipo;
	}

	public void setEquipo(Equipo equipo) {
		this.equipo = equipo;
	}

	public int getPGL() {
		return PGL;
	}

	public void setPGL(int pGL) {
		PGL = pGL;
	}

	public int getPPL() {
		return PPL;
	}

	public void setPPL(int pPL) {
		PPL = pPL;
	}

	public int getPEL() {
		return PEL;
	}

	public void setPEL(int pEL) {
		PEL = pEL;
	}

	public int getGFL() {
		return GFL;
	}

	public void setGFL(int gFL) {
		GFL = gFL;
	}

	public int getGCL() {
		return GCL;
	}

	public void setGCL(int gCL) {
		GCL = gCL;
	}

	public int getPGV() {
		return PGV;
	}

	public void setPGV(int pGV) {
		PGV = pGV;
	}

	public int getPPV() {
		return PPV;
	}

	public void setPPV(int pPV) {
		PPV = pPV;
	}

	public int getPEV() {
		return PEV;
	}

	public void setPEV(int pEV) {
		PEV = pEV;
	}

	public int getGFV() {
		return GFV;
	}

	public void setGFV(int gFV) {
		GFV = gFV;
	}

	public int getGCV() {
		return GCV;
	}

	public void setGCV(int gCV) {
		GCV = gCV;
	}

	public int getPuntosLocal() {
		return this.PGL * 3 + this.PEL;
	}
	
	public int getPuntosVisitante() {
		return this.PGV * 3 + this.PEV;
	}
	public int getGolesFavor() {
		return this.GFL + this.GFV;
	}
	
	public int getGolesContra() {
		return this.GCV  + this.GCL;
	}
	
	public int getGolesTotal() {
		return this.getGolesFavor()-this.getGolesContra();
		
	}
	public int getPuntosTotal() {
		return this.getPuntosLocal()+this.getPuntosVisitante();
	}

	
	

	@Override
	public String toString() {
		return "Clasificado [equipo=" + equipo + ", PGL=" + PGL + ", PPL=" + PPL + ", PEL=" + PEL + ", GVL=" + GFL
				+ ", GCL=" + GCL + ", PGV=" + PGV + ", PPV=" + PPV + ", PEV=" + PEV + ", GVV=" + GFV + ", GCV=" + GCV
				+ "]";
	}
	
	
	
}
