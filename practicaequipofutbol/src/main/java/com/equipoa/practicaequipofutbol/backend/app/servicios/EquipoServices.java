package com.equipoa.practicaequipofutbol.backend.app.servicios;

import java.util.List;

import com.equipoa.practicaequipofutbol.backend.modelo.Equipo;

public interface EquipoServices {
	
	public List<Equipo> findAll();
	
	public Equipo findOne(Integer idEquipo);

}
