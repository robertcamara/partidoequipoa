package com.equipoa.practicaequipofutbol.backend.app.servicios;

import java.util.List;

import com.equipoa.practicaequipofutbol.backend.modelo.Lance;
import com.equipoa.practicaequipofutbol.backend.modelo.TipoLance;


public interface LanceServices {
	
	public  List<Lance> findAll();
	
	public Lance findOne(Integer idLance);
	
	public List<Lance> getByTipoLance(String tipoLance);
	
	public void create(Integer equipo, Integer partido,  TipoLance tipoLance, Integer minuto,
			String comentario);

	public void delete(Integer id);
}
