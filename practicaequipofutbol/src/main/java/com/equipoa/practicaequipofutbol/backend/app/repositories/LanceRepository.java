package com.equipoa.practicaequipofutbol.backend.app.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.equipoa.practicaequipofutbol.backend.modelo.Equipo;
import com.equipoa.practicaequipofutbol.backend.modelo.Lance;
import com.equipoa.practicaequipofutbol.backend.modelo.Partido;

public interface LanceRepository extends JpaRepository<Lance, Integer> {
	
	public List<Lance> getByPartido(Partido partido);
	public List<Lance> getByEquipo(Equipo equipo);

}
