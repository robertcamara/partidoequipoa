package com.equipoa.practicaequipofutbol.backend.modelo;

public class Estadistica {

	private Partido partido;
	
	private int numAgresionesLocal;
	private int numAgresionesVisitante;

	private int numCornersLocal;
	private int numCornersVisitante;

	private int numFaltasLocal;
	private int numFaltasVisitante;

	private int numFaltasPeligrosasLocal;
	private int numFaltasPeligrosasVisitante;

	private int numGolesLocal;
	private int numGolesVisitante;

	private int numIncidenciasLocal;
	private int numIncidenciasVisitante;

	private int numJugadasLocal;
	private int numJugadasVisitante;

	private int numLesionesLocal;
	private int numLesionesVisitante;

	private int numOcasionGolLocal;
	private int numOcasionGolVisitante;

	private int numPenaltiesLocal;
	private int numPenaltiesVisitante;

	private int numTarjetaAmarillaLocal;
	private int numTarjetaAmarillaVisitante;

	private int numTarjetaRojaLocal;
	private int numTarjetaRojaVisitante;

	public Estadistica() {
	}

	public Estadistica(Partido partido, int numAgresionesLocal, int numAgresionesVisitante, int numCornersLocal,
			int numCornersVisitante, int numFaltasLocal, int numFaltasVisitante, int numFaltasPeligrosasLocal,
			int numFaltasPeligrosasVisitante, int numGolesLocal, int numGolesVisitante, int numIncidenciasLocal,
			int numIncidenciasVisitante, int numJugadasLocal, int numJugadasVisitante, int numLesionesLocal,
			int numLesionesVisitante, int numOcasionGolLocal, int numOcasionGolVisitante, int numPenaltiesLocal,
			int numPenaltiesVisitante, int numTarjetaAmarillaLocal, int numTarjetaAmarillaVisitante,
			int numTarjetaRojaLocal, int numTarjetaRojaVisitante) {
		this.partido = partido;
		this.numAgresionesLocal = numAgresionesLocal;
		this.numAgresionesVisitante = numAgresionesVisitante;
		this.numCornersLocal = numCornersLocal;
		this.numCornersVisitante = numCornersVisitante;
		this.numFaltasLocal = numFaltasLocal;
		this.numFaltasVisitante = numFaltasVisitante;
		this.numFaltasPeligrosasLocal = numFaltasPeligrosasLocal;
		this.numFaltasPeligrosasVisitante = numFaltasPeligrosasVisitante;
		this.numGolesLocal = numGolesLocal;
		this.numGolesVisitante = numGolesVisitante;
		this.numIncidenciasLocal = numIncidenciasLocal;
		this.numIncidenciasVisitante = numIncidenciasVisitante;
		this.numJugadasLocal = numJugadasLocal;
		this.numJugadasVisitante = numJugadasVisitante;
		this.numLesionesLocal = numLesionesLocal;
		this.numLesionesVisitante = numLesionesVisitante;
		this.numOcasionGolLocal = numOcasionGolLocal;
		this.numOcasionGolVisitante = numOcasionGolVisitante;
		this.numPenaltiesLocal = numPenaltiesLocal;
		this.numPenaltiesVisitante = numPenaltiesVisitante;
		this.numTarjetaAmarillaLocal = numTarjetaAmarillaLocal;
		this.numTarjetaAmarillaVisitante = numTarjetaAmarillaVisitante;
		this.numTarjetaRojaLocal = numTarjetaRojaLocal;
		this.numTarjetaRojaVisitante = numTarjetaRojaVisitante;
	}
	
	public Partido getPartido() {
		return partido;
	}

	public void setPartido(Partido partido) {
		this.partido = partido;
	}

	public int getNumAgresionesLocal() {
		return numAgresionesLocal;
	}

	public void setNumAgresionesLocal(int numAgresionesLocal) {
		this.numAgresionesLocal = numAgresionesLocal;
	}

	public int getNumAgresionesVisitante() {
		return numAgresionesVisitante;
	}

	public void setNumAgresionesVisitante(int numAgresionesVisitante) {
		this.numAgresionesVisitante = numAgresionesVisitante;
	}

	public int getNumCornersLocal() {
		return numCornersLocal;
	}

	public void setNumCornersLocal(int numCornersLocal) {
		this.numCornersLocal = numCornersLocal;
	}

	public int getNumCornersVisitante() {
		return numCornersVisitante;
	}

	public void setNumCornersVisitante(int numCornersVisitante) {
		this.numCornersVisitante = numCornersVisitante;
	}

	public int getNumFaltasLocal() {
		return numFaltasLocal;
	}

	public void setNumFaltasLocal(int numFaltasLocal) {
		this.numFaltasLocal = numFaltasLocal;
	}

	public int getNumFaltasVisitante() {
		return numFaltasVisitante;
	}

	public void setNumFaltasVisitante(int numFaltasVisitante) {
		this.numFaltasVisitante = numFaltasVisitante;
	}

	public int getNumFaltasPeligrosasLocal() {
		return numFaltasPeligrosasLocal;
	}

	public void setNumFaltasPeligrosasLocal(int numFaltasPeligrosasLocal) {
		this.numFaltasPeligrosasLocal = numFaltasPeligrosasLocal;
	}

	public int getNumFaltasPeligrosasVisitante() {
		return numFaltasPeligrosasVisitante;
	}

	public void setNumFaltasPeligrosasVisitante(int numFaltasPeligrosasVisitante) {
		this.numFaltasPeligrosasVisitante = numFaltasPeligrosasVisitante;
	}

	public int getNumGolesLocal() {
		return numGolesLocal;
	}

	public void setNumGolesLocal(int numGolesLocal) {
		this.numGolesLocal = numGolesLocal;
	}

	public int getNumGolesVisitante() {
		return numGolesVisitante;
	}

	public void setNumGolesVisitante(int numGolesVisitante) {
		this.numGolesVisitante = numGolesVisitante;
	}

	public int getNumIncidenciasLocal() {
		return numIncidenciasLocal;
	}

	public void setNumIncidenciasLocal(int numIncidenciasLocal) {
		this.numIncidenciasLocal = numIncidenciasLocal;
	}

	public int getNumIncidenciasVisitante() {
		return numIncidenciasVisitante;
	}

	public void setNumIncidenciasVisitante(int numIncidenciasVisitante) {
		this.numIncidenciasVisitante = numIncidenciasVisitante;
	}

	public int getNumJugadasLocal() {
		return numJugadasLocal;
	}

	public void setNumJugadasLocal(int numJugadasLocal) {
		this.numJugadasLocal = numJugadasLocal;
	}

	public int getNumJugadasVisitante() {
		return numJugadasVisitante;
	}

	public void setNumJugadasVisitante(int numJugadasVisitante) {
		this.numJugadasVisitante = numJugadasVisitante;
	}

	public int getNumLesionesLocal() {
		return numLesionesLocal;
	}

	public void setNumLesionesLocal(int numLesionesLocal) {
		this.numLesionesLocal = numLesionesLocal;
	}

	public int getNumLesionesVisitante() {
		return numLesionesVisitante;
	}

	public void setNumLesionesVisitante(int numLesionesVisitante) {
		this.numLesionesVisitante = numLesionesVisitante;
	}

	public int getNumOcasionGolLocal() {
		return numOcasionGolLocal;
	}

	public void setNumOcasionGolLocal(int numOcasionGolLocal) {
		this.numOcasionGolLocal = numOcasionGolLocal;
	}

	public int getNumOcasionGolVisitante() {
		return numOcasionGolVisitante;
	}

	public void setNumOcasionGolVisitante(int numOcasionGolVisitante) {
		this.numOcasionGolVisitante = numOcasionGolVisitante;
	}

	public int getNumPenaltiesLocal() {
		return numPenaltiesLocal;
	}

	public void setNumPenaltiesLocal(int numPenaltiesLocal) {
		this.numPenaltiesLocal = numPenaltiesLocal;
	}

	public int getNumPenaltiesVisitante() {
		return numPenaltiesVisitante;
	}

	public void setNumPenaltiesVisitante(int numPenaltiesVisitante) {
		this.numPenaltiesVisitante = numPenaltiesVisitante;
	}

	public int getNumTarjetaAmarillaLocal() {
		return numTarjetaAmarillaLocal;
	}

	public void setNumTarjetaAmarillaLocal(int numTarjetaAmarillaLocal) {
		this.numTarjetaAmarillaLocal = numTarjetaAmarillaLocal;
	}

	public int getNumTarjetaAmarillaVisitante() {
		return numTarjetaAmarillaVisitante;
	}

	public void setNumTarjetaAmarillaVisitante(int numTarjetaAmarillaVisitante) {
		this.numTarjetaAmarillaVisitante = numTarjetaAmarillaVisitante;
	}

	public int getNumTarjetaRojaLocal() {
		return numTarjetaRojaLocal;
	}

	public void setNumTarjetaRojaLocal(int numTarjetaRojaLocal) {
		this.numTarjetaRojaLocal = numTarjetaRojaLocal;
	}

	public int getNumTarjetaRojaVisitante() {
		return numTarjetaRojaVisitante;
	}

	public void setNumTarjetaRojaVisitante(int numTarjetaRojaVisitante) {
		this.numTarjetaRojaVisitante = numTarjetaRojaVisitante;
	}

	public int getAgresionesPorcentajeLocal() {
		
		if(numAgresionesLocal+numAgresionesVisitante != 0) {
			return (numAgresionesLocal*100)/(numAgresionesLocal+numAgresionesVisitante);
		}else {
			return 0;
		}		 
	}

	public int getAgresionesPorcentajeVisitante() {
		
		if(numAgresionesLocal+numAgresionesVisitante != 0) {
			return (numAgresionesVisitante*100)/(numAgresionesLocal+numAgresionesVisitante);
		}else {
			return 0;
		}	
	}
	
	public int getCornersPorcentajeLocal() {
		
		if(numCornersLocal+numCornersVisitante != 0) {
			return (numCornersLocal*100)/(numCornersLocal+numCornersVisitante);
		}else {
			return 0;
		}		 
	}

	public int getCornersPorcentajeVisitante() {

		if(numCornersLocal+numCornersVisitante != 0) {
			return (numCornersVisitante*100)/(numCornersLocal+numCornersVisitante);
		}else {
			return 0;
		}	
	}
	
	public int getFaltasPorcentajeLocal() {
		
		if(numFaltasLocal+numFaltasVisitante != 0) {
			return (numFaltasLocal*100)/(numFaltasLocal+numFaltasVisitante);
		}else {
			return 0;
		}		 
	}

	public int getFaltasPorcentajeVisitante() {

		if(numFaltasLocal+numFaltasVisitante != 0) {
			return (numFaltasVisitante*100)/(numFaltasLocal+numFaltasVisitante);
		}else {
			return 0;
		}
		
		
	}

	public int getFaltasPeligrosasPorcentajeLocal() {
		
		if(numFaltasPeligrosasLocal+numFaltasPeligrosasVisitante != 0) {
			return (numFaltasPeligrosasLocal*100)/(numFaltasPeligrosasLocal+numFaltasPeligrosasVisitante);
		}else {
			return 0;
		}
	}

	public int getFaltasPeligrosasPorcentajeVisitante() {
		
		if(numFaltasPeligrosasLocal+numFaltasPeligrosasVisitante != 0) {
			return (numFaltasPeligrosasVisitante*100)/(numFaltasPeligrosasLocal+numFaltasPeligrosasVisitante);
		}else {
			return 0;
		}
	}
	
	public int getGolesPorcentajeLocal() {
		
		if(numGolesLocal+numGolesVisitante != 0) {
			return (numGolesLocal*100)/(numGolesLocal+numGolesVisitante);
		}else {
			return 0;
		}	 
	}

	public int getGolesPorcentajeVisitante() {

		if(numGolesLocal+numGolesVisitante != 0) {	
			return (numGolesVisitante*100)/(numGolesLocal+numGolesVisitante);
		}else {
			return 0;
		}
	}
	 
	public int getIncidenciasPorcentajeLocal() {
		
		if(numIncidenciasLocal+numIncidenciasVisitante != 0) {	
			return (numIncidenciasLocal*100)/(numIncidenciasLocal+numIncidenciasVisitante);
		}else {
			return 0;
		} 
	}

	public int getIncidenciasPorcentajeVisitante() {

		if(numIncidenciasLocal+numIncidenciasVisitante != 0) {	
			return (numIncidenciasVisitante*100)/(numIncidenciasLocal+numIncidenciasVisitante);
		}else {
			return 0;
		} 
	}

	public int getJugadasPorcentajeLocal() {
		
		if(numJugadasLocal+numJugadasVisitante != 0) {	
			return (numJugadasLocal*100)/(numJugadasLocal+numJugadasVisitante);
		}else {
			return 0;
		}  
	}

	public int getJugadasPorcentajeVisitante() {
		
		if(numJugadasLocal+numJugadasVisitante != 0) {	
			return (numJugadasVisitante*100)/(numJugadasLocal+numJugadasVisitante);
		}else {
			return 0;
		}  
	}
	
	public int getLesionesPorcentajeLocal() {
		
		if(numLesionesLocal+numLesionesVisitante != 0) {	
			return (numLesionesLocal*100)/(numLesionesLocal+numLesionesVisitante);
		}else {
			return 0;
		} 	 
	}

	public int getLesionesPorcentajeVisitante() {
		
		if(numLesionesLocal+numLesionesVisitante != 0) {	
			return (numLesionesVisitante*100)/(numLesionesLocal+numLesionesVisitante);
		}else {
			return 0;
		} 	
	}
	
	public int getOcasionGolPorcentajeLocal() {
		
		if(numOcasionGolLocal+numOcasionGolVisitante != 0) {	
			return (numOcasionGolLocal*100)/(numOcasionGolLocal+numOcasionGolVisitante);
		}else {
			return 0;
		}  
	}

	public int getOcasionGolPorcentajeVisitante() {
		
		if(numOcasionGolLocal+numOcasionGolVisitante != 0) {	
			return (numOcasionGolVisitante*100)/(numOcasionGolLocal+numOcasionGolVisitante);
		}else {
			return 0;
		}  
		
	}
	
	public int getPenaltiesPorcentajeLocal() {
		
		if(numPenaltiesLocal+numPenaltiesVisitante != 0) {	
			return (numPenaltiesLocal*100)/(numPenaltiesLocal+numPenaltiesVisitante);
		}else {
			return 0;
		}  	 
	}

	public int getPenaltiesPorcentajeVisitante() {
		
		if(numPenaltiesLocal+numPenaltiesVisitante != 0) {	
			return (numPenaltiesVisitante*100)/(numPenaltiesLocal+numPenaltiesVisitante);
		}else {
			return 0;
		}  	
	}
	
	public int getTarjetaAmarillaPorcentajeLocal() {
		
		if(numTarjetaAmarillaLocal+numTarjetaAmarillaVisitante != 0) {	
			return (numTarjetaAmarillaLocal*100)/(numTarjetaAmarillaLocal+numTarjetaAmarillaVisitante);
		}else {
			return 0;
		}		 
	}

	public int getTarjetaAmarillaPorcentajeVisitante() {

		if(numTarjetaAmarillaLocal+numTarjetaAmarillaVisitante != 0) {	
			return (numTarjetaAmarillaVisitante*100)/(numTarjetaAmarillaLocal+numTarjetaAmarillaVisitante);
		}else {
			return 0;
		}	
	}
	
	public int getTarjetaRojaPorcentajeLocal() {
		
		if(numTarjetaRojaLocal+numTarjetaRojaVisitante != 0) {	
			return (numTarjetaRojaLocal*100)/(numTarjetaRojaLocal+numTarjetaRojaVisitante);
		}else {
			return 0;
		}	 
	}

	public int getTarjetaRojaPorcentajeVisitante() {
		
		if(numTarjetaRojaLocal+numTarjetaRojaVisitante != 0) {	
			return (numTarjetaRojaVisitante*100)/(numTarjetaRojaLocal+numTarjetaRojaVisitante);
		}else {
			return 0;
		}
		
	}

	@Override
	public String toString() {
		return "Estadistica [partido=" + partido + ", numAgresionesLocal=" + numAgresionesLocal
				+ ", numAgresionesVisitante=" + numAgresionesVisitante + ", numCornersLocal=" + numCornersLocal
				+ ", numCornersVisitante=" + numCornersVisitante + ", numFaltasLocal=" + numFaltasLocal
				+ ", numFaltasVisitante=" + numFaltasVisitante + ", numFaltasPeligrosasLocal="
				+ numFaltasPeligrosasLocal + ", numFaltasPeligrosasVisitante=" + numFaltasPeligrosasVisitante
				+ ", numGolesLocal=" + numGolesLocal + ", numGolesVisitante=" + numGolesVisitante
				+ ", numIncidenciasLocal=" + numIncidenciasLocal + ", numIncidenciasVisitante="
				+ numIncidenciasVisitante + ", numJugadasLocal=" + numJugadasLocal + ", numJugadasVisitante="
				+ numJugadasVisitante + ", numLesionesLocal=" + numLesionesLocal + ", numLesionesVisitante="
				+ numLesionesVisitante + ", numOcasionGolLocal=" + numOcasionGolLocal + ", numOcasionGolVisitante="
				+ numOcasionGolVisitante + ", numPenaltiesLocal=" + numPenaltiesLocal + ", numPenaltiesVisitante="
				+ numPenaltiesVisitante + ", numTarjetaAmarillaLocal=" + numTarjetaAmarillaLocal
				+ ", numTarjetaAmarillaVisitante=" + numTarjetaAmarillaVisitante + ", numTarjetaRojaLocal="
				+ numTarjetaRojaLocal + ", numTarjetaRojaVisitante=" + numTarjetaRojaVisitante + "]";
	}

}
