package com.equipoa.practicaequipofutbol.backend.api;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.equipoa.practicaequipofutbol.backend.app.servicios.LanceServices;
import com.equipoa.practicaequipofutbol.backend.app.servicios.Servicios;
import com.equipoa.practicaequipofutbol.backend.modelo.Lance;

@CrossOrigin
@Controller
@RequestMapping(value = "/diariodeportivo/api")
public class LanceController {

	@Autowired
	private Servicios servicios;
	
	@Autowired
	private LanceServices lanceServices;
	
		
	@RequestMapping(value = "/lances", 
					method = RequestMethod.GET, 
					produces = MediaType.APPLICATION_JSON_VALUE)
	public @ResponseBody List<Lance> getAll() {
		
		return lanceServices.findAll();
	}

	@RequestMapping(value="/lances/{id}",
			method=RequestMethod.GET,
			produces=MediaType.APPLICATION_JSON_VALUE)
	public @ResponseBody Lance getById(@PathVariable("id") Integer id) {
		
		return lanceServices.findOne(id);
	}
	@RequestMapping(value="/lances/partido/{id}",
			method=RequestMethod.GET,
			produces=MediaType.APPLICATION_JSON_VALUE)
	public @ResponseBody List<Lance> getByPartido(@PathVariable("id") Integer idPartido) {
		
		return servicios.getLancesByPartido(idPartido);

	}
	@RequestMapping(value="/lances/equipo/{id}",
			method=RequestMethod.GET,
			produces=MediaType.APPLICATION_JSON_VALUE)
	public @ResponseBody List<Lance> getByEquipo(@PathVariable("id") Integer idEquipo) {
		
		return servicios.getLancesByEquipo(idEquipo);

	}

	
	@RequestMapping(value="/lances/tipoLance/{tipo}",
			method=RequestMethod.GET,
			produces=MediaType.APPLICATION_JSON_VALUE)
	public @ResponseBody List<Lance> getByTipoLance(@PathVariable("tipo") String tipoLance) {
		
		return lanceServices.getByTipoLance(tipoLance);
	
	}
}
