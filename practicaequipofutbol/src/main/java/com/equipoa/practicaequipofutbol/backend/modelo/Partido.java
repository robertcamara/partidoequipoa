package com.equipoa.practicaequipofutbol.backend.modelo;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;


@Entity
@Table(name="PARTIDOS")
public class Partido implements Serializable{
	
	private static final long serialVersionUID = 1L;
	
	@Id
	private Integer id;
	
	private String jornada;
	
	@ManyToOne
	@JoinColumn(name="id_equipo_visitante")
	private Equipo visitante;
	
	@ManyToOne
	@JoinColumn(name="id_equipo_local")
	private Equipo local;
	
	private int golesLocal;
	private int golesVisitante;
	
	@Enumerated(EnumType.STRING)
	@Column(name="ID_ESTADO")
	private Estado estado;
	
	
	public Partido() {
		
	}
	
	public Partido(Integer id, String jornada, Equipo id_equipo_visitante, Equipo id_equipo_local, int golesLocal,
			int golesVisitante, Estado estadoPartido) {
		this.id = id;
		this.jornada = jornada;
		this.visitante = id_equipo_visitante;
		this.local = id_equipo_local;
		this.golesLocal = golesLocal;
		this.golesVisitante = golesVisitante;
		this.estado = estadoPartido;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getJornada() {
		return jornada;
	}

	public void setJornada(String jornada) {
		this.jornada = jornada;
	}

	public Equipo getVisitante() {
		return visitante;
	}

	public void setVisitante(Equipo visitante) {
		this.visitante = visitante;
	}

	public Equipo getLocal() {
		return local;
	}

	public void setLocal(Equipo local) {
		this.local = local;
	}

	public int getGolesLocal() {
		return golesLocal;
	}

	public void setGolesLocal(int golesLocal) {
		this.golesLocal = golesLocal;
	}

	public int getGolesVisitante() {
		return golesVisitante;
	}

	public void setGolesVisitante(int golesVisitante) {
		this.golesVisitante = golesVisitante;
	}

	public Estado getEstado() {
		return estado;
	}

	public void setEstado(Estado estado) {
		this.estado = estado;
	}

	@Override
	public String toString() {
		return "Partido [id=" + id + ", jornada=" + jornada + ", visitante=" + visitante + ", local=" + local
				+ ", golesLocal=" + golesLocal + ", golesVisitante=" + golesVisitante + ", estado=" + estado + "]";
	}

}
