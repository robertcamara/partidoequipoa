package com.equipoa.practicaequipofutbol.backend.app.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.equipoa.practicaequipofutbol.backend.app.servicios.EquipoServices;
import com.equipoa.practicaequipofutbol.backend.app.servicios.LanceServices;
import com.equipoa.practicaequipofutbol.backend.app.servicios.PartidoServices;
import com.equipoa.practicaequipofutbol.backend.app.servicios.Servicios;
import com.equipoa.practicaequipofutbol.backend.modelo.TipoLance;

@Controller
@RequestMapping(value = "diariodeportivo/app")
public class AppController {

	@Autowired
	private LanceServices lanceServices;

	@Autowired
	private PartidoServices partidoServices;

	@Autowired
	private EquipoServices equipoServices;

	@Autowired
	private Servicios servicios;

	
	
	// -------------HOME------------------	
	
	@RequestMapping(value="/",
			        method=RequestMethod.GET)
	public String home() {
		System.out.println("Entramos en home!!!!!");
		return "home";
	}
	
	
	
	// -------------EQUIPOS------------------

	@RequestMapping(value = "/equipos", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public String getAllEquipos(ModelMap modelMap) {

		modelMap.put("equipos", equipoServices.findAll());

		return "listadoEquipos";
	}

	@RequestMapping(value = "/equipo", method = RequestMethod.GET)
	public String getEquipoById(@RequestParam(name = "id") Integer id, ModelMap modelMap) {

		modelMap.put("equipo", equipoServices.findOne(id));

		return "fichaEquipo";
	}

	// -------------PARTIDOS------------------

	@RequestMapping(value = "/partidos", method = RequestMethod.GET)
	public String getAllPartidos(ModelMap modelMap) {
		
		modelMap.put("listadoPartidos", partidoServices.findAll());
	
		return "listadoPartidos";
	}

	@RequestMapping(value = "/partidos/jornada", method = RequestMethod.GET)
	public String getPartidosByJornada(@RequestParam(name = "id") String id, ModelMap modelMap) {
		
		int numJornadas = servicios.getNumeroJornadas();
		
		modelMap.put("numJornadas", numJornadas);
		modelMap.put("partidosJornada", partidoServices.getByJornada(id));
		return "partidosJornada";
	}

	@RequestMapping(value = "/partidos/equipo", method = RequestMethod.GET)
	public String getPartidosByEquipo(@RequestParam(name = "id") Integer id, ModelMap modelMap) {
		
		modelMap.put("partidosEquipo", servicios.getPartidosByEquipo(id));
    
		return "partidosEquipo";
	}

	@RequestMapping(value = "/clasificacion", method = RequestMethod.GET)
	public String getClasificacion(ModelMap modelMap) {

		modelMap.put("clasificacion", servicios.getClasificacion());
		return "clasificacion";
	}
	
	@RequestMapping(value = "/partidos/estado", method = RequestMethod.GET)
	public String setEstado(@RequestParam(name = "id") Integer id, ModelMap modelMap) {
		servicios.cambiarEstadoPartido(id);
		return "redirect:/diariodeportivo/app/partidos";
	}
	

	// -------------LANCES------------------

	@RequestMapping(value = "/lances", 
			method = RequestMethod.GET)
	public String getAllLances(ModelMap modelMap) {
		modelMap.put("lances",  lanceServices.findAll());
		return "listadoLances";

	}

	@RequestMapping(value = "/lance", 
			method = RequestMethod.GET)
	public String getLanceById(@RequestParam(name = "id") Integer id,ModelMap modelMap) {
		
		modelMap.put("fichaLance",lanceServices.findOne(id));
		return "listadoLances";

	}

	@RequestMapping(value = "/lances/partido",
					method = RequestMethod.GET)
	public String getLanceByPartido(@RequestParam(name = "id") Integer idPartido,ModelMap modelMap) {

		modelMap.put("partido", partidoServices.getPartido(idPartido));	
		modelMap.put("lancesPartido", servicios.getLancesByPartido(idPartido));	
		return "lancesPartido";

	}

	@RequestMapping(value = "/lances/equipo", 
					method = RequestMethod.GET)
	public String getLancesByEquipo(@RequestParam(name = "id") Integer id,ModelMap modelMap) {

		modelMap.put("lancesEquipo", servicios.getLancesByEquipo(id));
		return "lancesEquipo";

	}


	@RequestMapping(value = "/lances/tipoLance", 
			method = RequestMethod.GET)
	public String getByTipoLance(@RequestParam(name = "tipo") String tipoLance,ModelMap modelMap) {

		modelMap.put("lancesTipados",lanceServices.getByTipoLance(tipoLance));
		
		return "lancesTipados";

	}
	
	@RequestMapping(value="/lances",
			method=RequestMethod.POST)
	public String create(
						 @RequestParam(name = "equipo") Integer equipo, 
						 @RequestParam(name = "partido") Integer partido,
						 @RequestParam(name = "tipoLance") TipoLance tipoLance,
						 @RequestParam(name = "minuto") Integer minuto,
						 @RequestParam(name = "comentario") String comentario,
						 ModelMap modelMap) {
		
		lanceServices.create(equipo, partido, tipoLance,minuto,comentario);
		modelMap.put("partido", partidoServices.getPartido(partido));	
		modelMap.put("lancesPartido", servicios.getLancesByPartido(partido));	
		return "lancesPartido";
		

	}
	
	@RequestMapping(value="/lances",
			method=RequestMethod.DELETE)
	public String deleteLanceByid(@RequestParam(name = "id") Integer id) {
		lanceServices.delete(id);
		return "listadoLances";
	}
	
	@RequestMapping(value="/estadisticas/partido",
			method=RequestMethod.GET)
	public String getEstadisticas(@RequestParam(name = "id") Integer id, ModelMap modelMap) {
		modelMap.put("estadistica", servicios.getEstadistica(id));
		return "estadisticas";
	}
	
	
}
