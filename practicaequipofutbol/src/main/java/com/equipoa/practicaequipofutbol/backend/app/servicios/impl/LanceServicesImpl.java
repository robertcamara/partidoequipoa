package com.equipoa.practicaequipofutbol.backend.app.servicios.impl;

import java.util.ArrayList;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.equipoa.practicaequipofutbol.backend.app.repositories.EquipoRepository;
import com.equipoa.practicaequipofutbol.backend.app.repositories.LanceRepository;
import com.equipoa.practicaequipofutbol.backend.app.repositories.PartidoRepository;
import com.equipoa.practicaequipofutbol.backend.app.servicios.EquipoServices;
import com.equipoa.practicaequipofutbol.backend.app.servicios.LanceServices;
import com.equipoa.practicaequipofutbol.backend.app.servicios.PartidoServices;
import com.equipoa.practicaequipofutbol.backend.app.servicios.Servicios;
import com.equipoa.practicaequipofutbol.backend.modelo.Equipo;
import com.equipoa.practicaequipofutbol.backend.modelo.Lance;
import com.equipoa.practicaequipofutbol.backend.modelo.Partido;
import com.equipoa.practicaequipofutbol.backend.modelo.TipoLance;

@Service
public class LanceServicesImpl implements LanceServices {

	@Autowired
	private LanceRepository lanceRepository;
	
	@Autowired
	private EquipoServices equipoServices;
	
	@Autowired
	private PartidoServices partidoServices;
	
	@Autowired
	private Servicios services;
	
	
	@Override
	public List<Lance> findAll() {
		return lanceRepository.findAll();
	}

	@Override
	public Lance findOne(Integer idLance) {
		
		return lanceRepository.findOne(idLance);
	}

	
	@Override
	public List<Lance> getByTipoLance(String tipoLance) {
		List<Lance> lancesTipados = new ArrayList<Lance>(); 
		List<Lance> allLances = lanceRepository.findAll();
		
		for(Lance lance:allLances) {
			if(lance.getTipo().equals(tipoLance)) {
				lancesTipados.add(lance);		
			}
		}
		
		return lancesTipados;
	}

	
	
	
	
	
	@Override
	public void create(Integer equipo, Integer partido,  TipoLance tipoLance, Integer minuto,
			String comentario) {
		int codigoAleatorio = (int)(Math.random()*10000);
		Equipo e = equipoServices.findOne(equipo);
		Partido p = partidoServices.getPartido(partido);
		System.out.println(p.getGolesLocal()+e.getNombre());
		Lance lance = new Lance(codigoAleatorio,e,p,tipoLance,minuto,comentario);
		lanceRepository.save(lance);
		services.isGol(lance);
	}

	
	
	
	
	
	
	
	
	@Override
	public void delete(Integer id) {
		lanceRepository.delete(id);
		
	}

}
