package com.equipoa.practicaequipofutbol.backend.api;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;


import com.equipoa.practicaequipofutbol.backend.app.servicios.EquipoServices;
import com.equipoa.practicaequipofutbol.backend.modelo.Equipo;


@CrossOrigin
@Controller
@RequestMapping(value="diariodeportivo/api")
public class EquipoController {

	
	@Autowired
	private EquipoServices equipoServices;
	
	@RequestMapping(value="/equipos", 
					method=RequestMethod.GET,
					produces=MediaType.APPLICATION_JSON_VALUE)
	public @ResponseBody List<Equipo> getAll(){
		
		return equipoServices.findAll();
		
	}
	
	@RequestMapping(value="/equipo/{id}",
					method=RequestMethod.GET,
					produces=MediaType.APPLICATION_JSON_VALUE)
	public @ResponseBody Equipo getByNombre(@PathVariable("id") Integer id) {
		
		return equipoServices.findOne(id);
	}
	
	
	
}
