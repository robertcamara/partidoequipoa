package com.equipoa.practicaequipofutbol.backend.modelo;

import java.util.Comparator;

public class ComparadorClasificados implements Comparator<Clasificado>{

	@Override
	public int compare(Clasificado equipo, Clasificado equipo2) {
		
		if(equipo.getPuntosTotal()==equipo2.getPuntosTotal()) {	
			if(equipo.getGolesTotal()<equipo2.getGolesTotal())return 1;
			else if(equipo.getGolesTotal()>equipo2.getGolesTotal())return -1;
			else{
				return equipo.getEquipo().getNombre().compareTo(equipo2.getEquipo().getNombre());		
				
			}
		}
		
		else if(equipo.getPuntosTotal()<equipo2.getPuntosTotal()) {
			return 1;
			
		}else {
			return -1;
		}
	}

}
