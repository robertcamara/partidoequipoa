package com.equipoa.practicaequipofutbol.backend.app.servicios.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.equipoa.practicaequipofutbol.backend.app.repositories.EquipoRepository;
import com.equipoa.practicaequipofutbol.backend.app.repositories.LanceRepository;
import com.equipoa.practicaequipofutbol.backend.app.repositories.PartidoRepository;
import com.equipoa.practicaequipofutbol.backend.app.servicios.Servicios;
import com.equipoa.practicaequipofutbol.backend.modelo.Clasificado;
import com.equipoa.practicaequipofutbol.backend.modelo.ComparadorClasificados;
import com.equipoa.practicaequipofutbol.backend.modelo.Equipo;
import com.equipoa.practicaequipofutbol.backend.modelo.Estadistica;
import com.equipoa.practicaequipofutbol.backend.modelo.Estado;
import com.equipoa.practicaequipofutbol.backend.modelo.Lance;
import com.equipoa.practicaequipofutbol.backend.modelo.Partido;
import com.equipoa.practicaequipofutbol.backend.modelo.TipoLance;

@Service
public class ServiciosImpl implements Servicios {

	@Autowired
	private PartidoRepository partidoRepository;
	
	@Autowired
	private EquipoRepository equipoRepository;
	
	@Autowired
	private LanceRepository lanceRepository;
	    
	
	@Override
	public List<Clasificado> getClasificacion() {
		
		List<Equipo> equipos = this.equipoRepository.findAll(); 
		
		ComparadorClasificados c = new ComparadorClasificados();
		
		Map<Equipo,Clasificado> mapeo = new HashMap<Equipo,Clasificado>();	
		
		for(Equipo equipo: equipos) {
			Clasificado clasificado = new Clasificado();
			clasificado.setEquipo(equipo);
			mapeo.put(equipo,clasificado);
		}
		
		List<Partido> partidos = this.partidoRepository.getPartidosPuntuables();
		
		List<Clasificado> listaClasificados =  new ArrayList<Clasificado>(mapeo.values());
			
		for(Partido partido: partidos) {
			
			Clasificado clasiLocal = mapeo.get(partido.getLocal());
			Clasificado clasiVisitante = mapeo.get(partido.getVisitante());
			
			clasiLocal.setGCL(clasiLocal.getGCL() + partido.getGolesVisitante());
			clasiVisitante.setGCV(clasiLocal.getGCV() + partido.getGolesLocal());
			clasiLocal.setGFL(clasiLocal.getGFL() + partido.getGolesLocal());
			clasiVisitante.setGFV(clasiVisitante.getGFV() + partido.getGolesVisitante());
			
			if(partido.getGolesLocal()==partido.getGolesVisitante()) {
				clasiLocal.setPEL(clasiLocal.getPEL() + 1);
				clasiVisitante.setPEV(clasiVisitante.getPEV() + 1);
			}
			if(partido.getGolesLocal()< partido.getGolesVisitante()) {
				clasiVisitante.setPGV(clasiVisitante.getPGV() + 1);
				clasiLocal.setPPL(clasiLocal.getPPL() + 1);	
			}
			if(partido.getGolesLocal()> partido.getGolesVisitante()) {
				clasiLocal.setPGL(clasiLocal.getPGL() + 1);
				clasiVisitante.setPPV(clasiVisitante.getPPV() + 1);
			}
		}	
		listaClasificados.sort(c);
		return listaClasificados ;
	}


	@Override
	public List<Lance> getLancesByPartido(Integer idPartido) {
		
		List<Lance> allLances = lanceRepository.findAll();
		List<Lance> lancesPartido = new ArrayList<Lance>();
		for(Lance lance:allLances) {
			if(idPartido==lance.getPartido().getId()) {
				lancesPartido.add(lance);
			}
		}
		
		return lancesPartido;
	}


	@Override
	public int getNumeroJornadas() {
		int numeroEquipos = (int) this.equipoRepository.count();
		return (numeroEquipos * (numeroEquipos -1)) / (numeroEquipos/2);
	}


	@Override
	public void cambiarEstadoPartido(Partido partido) {
		Estado estado = partido.getEstado();
		if(estado.equals(Estado.PENDIENTE)) {
			partido.setEstado(Estado.ABIERTO);
		}
		if(estado.equals(Estado.ABIERTO)) {
			partido.setEstado(Estado.CERRADO);
		}
		partidoRepository.save(partido);	
	}


	@Override
	public void cambiarEstadoPartido(int codigoPartido) {
		Partido partido = partidoRepository.findOne(codigoPartido);
		cambiarEstadoPartido(partido);	
	}
	
	public int isGol(Lance lance) {
		
		Partido partido = lance.getPartido();
		Equipo equipo = lance.getEquipo();
		
		if(lance.getTipo().equals(TipoLance.GOL)) {
			
			if(equipo.equals(partido.getLocal())) {
				partido.setGolesLocal(partido.getGolesLocal()+1);
				partidoRepository.save(partido);
				return partido.getGolesLocal();
			}
			
			if(equipo.equals(partido.getVisitante())) {
				partido.setGolesVisitante(partido.getGolesVisitante()+1);
				partidoRepository.save(partido);
				return partido.getGolesVisitante();
			}
		}
		
		return 0;
		
	}





	@Override
	public List<Partido> getPartidosByEquipo(Integer idEquipo) {
		List<Partido> allPartidos = partidoRepository.findAll();
		List<Partido>  partidosEquipo = new ArrayList<Partido>();
		for(Partido partido:allPartidos) {
			if(idEquipo==partido.getLocal().getId()||idEquipo==partido.getVisitante().getId()) {
				partidosEquipo.add(partido);
			}
		}
		
		return partidosEquipo;
	}
		
	@Override
	public List<Lance> getLancesByEquipo(Integer idEquipo) {
		List<Lance> allLances = lanceRepository.findAll();
		List<Lance> lancesEquipo = new ArrayList<Lance>();
		for(Lance lance:allLances) {
			if(idEquipo==lance.getEquipo().getId()) {
				lancesEquipo.add(lance);
			}
		}
		
		return lancesEquipo;
		}
	
	@Override
	public Estadistica getEstadistica(int id) {
		List<Lance> listadoLances = lanceRepository.getByPartido(partidoRepository.findOne(id));
		Estadistica estadistica = new Estadistica();
		estadistica.setPartido(partidoRepository.findOne(id)); 
		Equipo equipoLocal = estadistica.getPartido().getLocal();
		Equipo equipoVisitante = estadistica.getPartido().getVisitante();
		
		for (Lance lance : listadoLances) {
			if(lance.getTipo().equals(TipoLance.AGRESIÓN)){
				if(lance.getEquipo().equals(equipoLocal))  {
					estadistica.setNumAgresionesLocal(estadistica.getNumAgresionesLocal() + 1);
				}
				if(lance.getEquipo().equals(equipoVisitante)) {
					estadistica.setNumAgresionesVisitante(estadistica.getNumAgresionesVisitante() + 1);
				}
			}

			if(lance.getTipo().equals(TipoLance.CORNER)){
				if(lance.getEquipo().equals(equipoLocal))  {
					estadistica.setNumCornersLocal(estadistica.getNumCornersLocal() + 1);
				}
				if(lance.getEquipo().equals(equipoVisitante)) {
					estadistica.setNumCornersVisitante(estadistica.getNumCornersVisitante() + 1);
				}
			}
			
			if(lance.getTipo().equals(TipoLance.FALTA)){
				if(lance.getEquipo().equals(equipoLocal))  {
					estadistica.setNumFaltasLocal(estadistica.getNumFaltasLocal() + 1);
				}
				if(lance.getEquipo().equals(equipoVisitante)) {
					estadistica.setNumFaltasVisitante(estadistica.getNumFaltasVisitante() + 1);
				}
			}
			
			if(lance.getTipo().equals(TipoLance.FALTA_PELIGROSA)){
				if(lance.getEquipo().equals(equipoLocal))  {
					estadistica.setNumFaltasPeligrosasLocal(estadistica.getNumFaltasPeligrosasLocal() + 1);
				}
				if(lance.getEquipo().equals(equipoVisitante)) {
					estadistica.setNumFaltasPeligrosasVisitante(estadistica.getNumFaltasPeligrosasVisitante() + 1);
				}
			}
			
			if(lance.getTipo().equals(TipoLance.GOL)){
				if(lance.getEquipo().equals(equipoLocal))  {
					estadistica.setNumGolesLocal(estadistica.getNumGolesLocal() + 1);
				}
				if(lance.getEquipo().equals(equipoVisitante)) {
					estadistica.setNumGolesVisitante(estadistica.getNumGolesVisitante() + 1);
				}
			}
			
			if(lance.getTipo().equals(TipoLance.INCIDENCIA)){
				if(lance.getEquipo().equals(equipoLocal))  {
					estadistica.setNumIncidenciasLocal(estadistica.getNumIncidenciasLocal() + 1);
				}
				if(lance.getEquipo().equals(equipoVisitante)) {
					estadistica.setNumIncidenciasVisitante(estadistica.getNumIncidenciasVisitante() + 1);
				}
			}
			
			if(lance.getTipo().equals(TipoLance.JUGADA)){
				if(lance.getEquipo().equals(equipoLocal))  {
					estadistica.setNumJugadasLocal(estadistica.getNumJugadasLocal() + 1);
				}
				if(lance.getEquipo().equals(equipoVisitante)) {
					estadistica.setNumJugadasVisitante(estadistica.getNumJugadasVisitante() + 1);
				}
			}
			
			if(lance.getTipo().equals(TipoLance.LESIÓN)){
				if(lance.getEquipo().equals(equipoLocal))  {
					estadistica.setNumLesionesLocal(estadistica.getNumLesionesLocal() + 1);
				}
				if(lance.getEquipo().equals(equipoVisitante)) {
					estadistica.setNumLesionesVisitante(estadistica.getNumLesionesVisitante() + 1);
				}
			}
			
			if(lance.getTipo().equals(TipoLance.OCASIÓN)){
				if(lance.getEquipo().equals(equipoLocal))  {
					estadistica.setNumOcasionGolLocal(estadistica.getNumOcasionGolLocal() + 1);
				}
				if(lance.getEquipo().equals(equipoVisitante)) {
					estadistica.setNumOcasionGolVisitante(estadistica.getNumOcasionGolVisitante() + 1);
				}
			}
			
			if(lance.getTipo().equals(TipoLance.PENALTY)){
				if(lance.getEquipo().equals(equipoLocal))  {
					estadistica.setNumPenaltiesLocal(estadistica.getNumPenaltiesLocal() + 1);
				}
				if(lance.getEquipo().equals(equipoVisitante)) {
					estadistica.setNumPenaltiesVisitante(estadistica.getNumPenaltiesVisitante() + 1);
				}
			}
			
			if(lance.getTipo().equals(TipoLance.TARJETA_AMARILLA)){
				if(lance.getEquipo().equals(equipoLocal))  {
					estadistica.setNumTarjetaAmarillaLocal(estadistica.getNumTarjetaAmarillaLocal() + 1);
				}
				if(lance.getEquipo().equals(equipoVisitante)) {
					estadistica.setNumTarjetaAmarillaVisitante(estadistica.getNumTarjetaAmarillaVisitante() + 1);
				}
			}
			
			if(lance.getTipo().equals(TipoLance.TARJETA_ROJA)){
				if(lance.getEquipo().equals(equipoLocal))  {
					estadistica.setNumTarjetaRojaLocal(estadistica.getNumTarjetaRojaLocal() + 1);
				}
				if(lance.getEquipo().equals(equipoVisitante)) {
					estadistica.setNumTarjetaRojaVisitante(estadistica.getNumTarjetaRojaVisitante() + 1);
				}
			}
			
		}
		
		return estadistica;
	
		}
	}