package com.equipoa.practicaequipofutbol.backend.app.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.equipoa.practicaequipofutbol.backend.modelo.Equipo;

@Repository
public interface EquipoRepository extends JpaRepository<Equipo, Integer>{

	public Equipo getByNombre(String nombre);
}
