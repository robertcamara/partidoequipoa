package com.equipoa.practicaequipofutbol.backend.modelo;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="LANCES")
public class Lance implements Serializable{
	
	private static final long serialVersionUID = 1L;
	
	@Id
	private Integer id;
	
	@ManyToOne
	@JoinColumn(name="ID_equipo")
	private Equipo equipo;
	
	@ManyToOne
	@JoinColumn(name="ID_partido")
	private Partido partido;
	
	@Enumerated(EnumType.STRING)
	@Column(name="id_tipo")
	private TipoLance tipo;
	private Integer minuto;
	private String comentario;
	
	public Lance() {
		
	}

	public Lance(Integer id,Equipo equipo, Partido partido, TipoLance tipo, Integer minuto, String comentario) {
		this.id=id;
		this.equipo = equipo;
		this.partido = partido;
		this.tipo = tipo;
		this.minuto = minuto;
		this.comentario = comentario;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Equipo getEquipo() {
		return equipo;
	}

	public void setEquipo(Equipo equipo) {
		this.equipo = equipo;
	}

	public Partido getPartido() {
		return partido;
	}

	public void setPartido(Partido partido) {
		this.partido = partido;
	}

	public TipoLance getTipo() {
		return tipo;
	}

	public void setTipo(TipoLance tipo) {
		this.tipo = tipo;
	}

	public Integer getMinuto() {
		return minuto;
	}

	public void setMinuto(Integer minuto) {
		this.minuto = minuto;
	}

	public String getComentario() {
		return comentario;
	}

	public void setComentario(String comentario) {
		this.comentario = comentario;
	}

	@Override
	public String toString() {
		return "Lance [id=" + id + ", equipo=" + equipo + ", partido=" + partido + ", tipo=" + tipo + ", minuto="
				+ minuto + ", comentario=" + comentario + "]";
	}

}
