package com.equipoa.practicaequipofutbol.backend.app.servicios.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.equipoa.practicaequipofutbol.backend.app.repositories.PartidoRepository;
import com.equipoa.practicaequipofutbol.backend.app.servicios.PartidoServices;
import com.equipoa.practicaequipofutbol.backend.modelo.Partido;

@Service
public class PartidoServicesImpl implements PartidoServices {

	@Autowired
	private PartidoRepository partidoRepository;
	
	@Override
	public List<Partido> findAll() {		
		return partidoRepository.findAll();
	}

	@Override
	public Partido getPartido(Integer idPartido) {
		
		return partidoRepository.findOne(idPartido);
	}

	@Override
	public List<Partido> getByJornada(String jornada) {
		
		return partidoRepository.getByJornada(jornada);
	}

}
