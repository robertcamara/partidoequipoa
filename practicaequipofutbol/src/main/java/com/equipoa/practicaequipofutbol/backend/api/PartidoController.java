package com.equipoa.practicaequipofutbol.backend.api;

import java.util.Collection;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.equipoa.practicaequipofutbol.backend.app.servicios.PartidoServices;
import com.equipoa.practicaequipofutbol.backend.app.servicios.Servicios;
import com.equipoa.practicaequipofutbol.backend.modelo.Clasificado;
import com.equipoa.practicaequipofutbol.backend.modelo.Partido;

@CrossOrigin
@RequestMapping(value="/diariodeportivo/api")
@Controller
public class PartidoController {

	@Autowired
	private PartidoServices partidoServices;
	
	@Autowired
	private Servicios servicios;
	
	@RequestMapping(value="/partidos",
					method=RequestMethod.GET,
					produces=MediaType.APPLICATION_JSON_VALUE)
	public @ResponseBody List<Partido> getAll(){
		
		return partidoServices.findAll();
	}
	
	@RequestMapping(value="/partidos/jornada/{id}",
			method=RequestMethod.GET,
			produces=MediaType.APPLICATION_JSON_VALUE)
	public @ResponseBody List<Partido> getByJornada(@PathVariable("id") String idJornada){
	
	return partidoServices.getByJornada(idJornada);
}
	
	@RequestMapping(value="/partidos/equipo/{id}",
			method=RequestMethod.GET,
			produces=MediaType.APPLICATION_JSON_VALUE)
	public @ResponseBody List<Partido> getByEquipo(@PathVariable("id") Integer idEquipo){
	
	return servicios.getPartidosByEquipo(idEquipo);
}
	@RequestMapping(value="/clasificacion",
			method=RequestMethod.GET,
			produces=MediaType.APPLICATION_JSON_VALUE)
	public @ResponseBody Collection<Clasificado> getClasificacion(){
		
			
		return servicios.getClasificacion();
	}
	
	

	
}
