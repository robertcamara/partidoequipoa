package com.equipoa.practicaequipofutbol.backend.modelo;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="EQUIPOS")
public class Equipo implements Serializable{
	private static final long serialVersionUID = 1L;
	
	@Id
	private Integer id;
	
	private String nombre;
	private String escudo;
	
	public Equipo() {
		
	}

	public Equipo(Integer id, String nombre, String escudo) {
		this.id = id;
		this.nombre = nombre;
		this.escudo = escudo;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getEscudo() {
		return escudo;
	}

	public void setEscudo(String escudo) {
		this.escudo = escudo;
	}
	
	@Override
	public String toString() {
		return "Equipo [id=" + id + ", nombre=" + nombre + ", escudo=" + escudo + "]";
	}

}
