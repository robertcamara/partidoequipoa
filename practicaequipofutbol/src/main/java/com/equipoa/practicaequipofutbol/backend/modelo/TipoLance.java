package com.equipoa.practicaequipofutbol.backend.modelo;

public enum TipoLance {

	GOL,
	TARJETA_AMARILLA,
	TARJETA_ROJA,
	AGRESIÓN,
	CORNER,
	PENALTY,
	INCIDENCIA,
	OCASIÓN,
	LESIÓN,
	JUGADA,
	FALTA,
	FALTA_PELIGROSA,
	EXPULSION

}
