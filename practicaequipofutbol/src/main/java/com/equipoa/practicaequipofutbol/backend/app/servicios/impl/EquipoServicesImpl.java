package com.equipoa.practicaequipofutbol.backend.app.servicios.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.equipoa.practicaequipofutbol.backend.app.repositories.EquipoRepository;
import com.equipoa.practicaequipofutbol.backend.app.servicios.EquipoServices;
import com.equipoa.practicaequipofutbol.backend.modelo.Equipo;

@Service
public class EquipoServicesImpl implements EquipoServices {

	@Autowired
	private EquipoRepository equipoRepository;
	
	@Override
	public List<Equipo> findAll() {
		
		return equipoRepository.findAll();
	}

	@Override
	public Equipo findOne(Integer idEquipo) {
		
		return equipoRepository.findOne(idEquipo);
	}

}
