package com.equipoa.practicaequipofutbol.backend.app.servicios;

import java.util.List;

import com.equipoa.practicaequipofutbol.backend.modelo.Partido;

public interface PartidoServices{
	
	public List<Partido> findAll();
	
	public Partido getPartido(Integer idPartido);
	
	public List<Partido> getByJornada(String jornada);
}
