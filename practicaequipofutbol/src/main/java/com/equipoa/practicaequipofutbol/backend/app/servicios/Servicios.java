package com.equipoa.practicaequipofutbol.backend.app.servicios;


import java.util.List;

import com.equipoa.practicaequipofutbol.backend.modelo.Clasificado;
import com.equipoa.practicaequipofutbol.backend.modelo.Estadistica;
import com.equipoa.practicaequipofutbol.backend.modelo.Lance;
import com.equipoa.practicaequipofutbol.backend.modelo.Partido;


public interface Servicios {

	public List<Clasificado> getClasificacion();
	
	
	
	public int getNumeroJornadas();
	
	public void cambiarEstadoPartido(Partido partido);
	public void cambiarEstadoPartido(int codigoPartido);
	
	public int isGol(Lance lance);
	
	public List<Lance> getLancesByPartido(Integer idPartido);
	public List<Lance> getLancesByEquipo(Integer idPartido);
	
	
	public List<Partido> getPartidosByEquipo(Integer idEquipo);
	
	public Estadistica getEstadistica(int id);
	
}
