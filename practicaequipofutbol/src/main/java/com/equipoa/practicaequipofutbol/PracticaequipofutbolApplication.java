package com.equipoa.practicaequipofutbol;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PracticaequipofutbolApplication {

	public static void main(String[] args) {
		SpringApplication.run(PracticaequipofutbolApplication.class, args);
	}
}
    