LOCK TABLES `equipos` WRITE;
/*!40000 ALTER TABLE `equipos` DISABLE KEYS */;
INSERT INTO `equipos` VALUES (1,'FC BARCELONA','a.jpg'),(2,'R.MADRID','b.jpg'),(3,'AT. MADRID','c.jsp'),(4,'VALENCIA','d.jsp'),(5,'VILLAREAL','e.jsp'),(6,'SEVILLA','f.jsp'),(7,'EIBAR','g.jsp'),(8,'BETIS','h.jsp'),(9,'CELTA','i.jsp'),(10,'GIRONA','j.jsp'),(11,'GETAFE','k.jsp'),(12,'LEGANÉS','l.jsp'),(13,'ATH.BILBAO','m.jsp'),(14,'R.SOCIEDAD','n.jsp'),(15,'ESPANYOL','o.jsp'),(16,'ALAVÉS','p.jsp'),(17,'LEVANTE','q.jsp'),(18,'LAS PALMAS','r.jsp'),(19,'DEPORTIVO','s.jsp'),(20,'MÁLAGA','t.jsp');
/*!40000 ALTER TABLE `equipos` ENABLE KEYS */;
UNLOCK TABLES;


LOCK TABLES `estados` WRITE;
/*!40000 ALTER TABLE `estados` DISABLE KEYS */;
INSERT INTO `estados` VALUES ('ABIERTO'),('CERRADO'),('PENDIENTE');
/*!40000 ALTER TABLE `estados` ENABLE KEYS */;
UNLOCK TABLES;

LOCK TABLES `lances` WRITE;
/*!40000 ALTER TABLE `lances` DISABLE KEYS */;
/*!40000 ALTER TABLE `lances` ENABLE KEYS */;
UNLOCK TABLES;

LOCK TABLES `partidos` WRITE;
/*!40000 ALTER TABLE `partidos` DISABLE KEYS */;
INSERT INTO `partidos` VALUES (1,'1',14,5,0,0,'PENDIENTE'),(2,'1',16,6,0,0,'PENDIENTE'),(3,'1',10,12,0,0,'PENDIENTE'),(4,'1',8,13,0,0,'PENDIENTE'),(5,'1',2,3,0,0,'PENDIENTE'),(6,'1',4,19,0,0,'PENDIENTE'),(7,'1',1,7,0,0,'PENDIENTE'),(8,'1',20,9,0,0,'PENDIENTE'),(9,'1',15,17,0,0,'PENDIENTE'),(10,'1',11,18,0,0,'PENDIENTE');
INSERT INTO `partidos` VALUES (11,'2',6,5,0,0,'PENDIENTE'),(12,'2',14,10,0,0,'PENDIENTE'),(13,'2',13,16,0,0,'PENDIENTE'),(14,'2',12,2,0,0,'PENDIENTE'),(15,'2',19,8,0,0,'PENDIENTE'),(16,'2',3,1,0,0,'PENDIENTE'),(17,'2',9,4,0,0,'PENDIENTE'),(18,'2',7,15,0,0,'PENDIENTE'),(19,'2',18,20,0,0,'PENDIENTE'),(20,'2',17,11,0,0,'PENDIENTE');
INSERT INTO `partidos` VALUES (21,'3',10,5,0,0,'PENDIENTE'),(22,'3',6,13,0,0,'PENDIENTE'),(23,'3',2,14,0,0,'PENDIENTE'),(24,'3',16,19,0,0,'PENDIENTE'),(25,'3',1,12,0,0,'PENDIENTE'),(26,'3',8,9,0,0,'PENDIENTE'),(27,'3',15,3,0,0,'PENDIENTE'),(28,'3',4,18,0,0,'PENDIENTE'),(29,'3',11,7,0,0,'PENDIENTE'),(30,'3',20,17,0,0,'PENDIENTE');
INSERT INTO `partidos` VALUES (31,'4',13,5,0,0,'PENDIENTE'),(32,'4',10,2,0,0,'PENDIENTE'),(33,'4',19,6,0,0,'PENDIENTE'),(34,'4',14,1,0,0,'PENDIENTE'),(35,'4',9,16,0,0,'PENDIENTE'),(36,'4',12,15,0,0,'PENDIENTE'),(37,'4',18,8,0,0,'PENDIENTE'),(38,'4',3,11,0,0,'PENDIENTE'),(39,'4',17,4,0,0,'PENDIENTE'),(40,'4',7,20,0,0,'PENDIENTE');
INSERT INTO `partidos` VALUES (41,'5',2,5,0,0,'PENDIENTE'),(42,'5',13,19,0,0,'PENDIENTE'),(43,'5',1,10,0,0,'PENDIENTE'),(44,'5',6,9,0,0,'PENDIENTE'),(45,'5',15,14,0,0,'PENDIENTE'),(46,'5',16,18,0,0,'PENDIENTE'),(47,'5',11,12,0,0,'PENDIENTE'),(48,'5',8,17,0,0,'PENDIENTE'),(49,'5',20,3,0,0,'PENDIENTE'),(50,'5',4,7,0,0,'PENDIENTE');
INSERT INTO `partidos` VALUES (51,'6',19,5,0,0,'PENDIENTE'),(52,'6',2,1,0,0,'PENDIENTE'),(53,'6',9,13,0,0,'PENDIENTE'),(54,'6',10,15,0,0,'PENDIENTE'),(55,'6',18,6,0,0,'PENDIENTE'),(56,'6',14,11,0,0,'PENDIENTE'),(57,'6',17,16,0,0,'PENDIENTE'),(58,'6',12,20,0,0,'PENDIENTE'),(59,'6',7,8,0,0,'PENDIENTE'),(60,'6',3,4,0,0,'PENDIENTE');
INSERT INTO `partidos` VALUES (61,'7',1,5,0,0,'PENDIENTE'),(62,'7',19,9,0,0,'PENDIENTE'),(63,'7',15,2,0,0,'PENDIENTE'),(64,'7',13,18,0,0,'PENDIENTE'),(65,'7',11,10,0,0,'PENDIENTE'),(66,'7',6,17,0,0,'PENDIENTE'),(67,'7',20,14,0,0,'PENDIENTE'),(68,'7',16,7,0,0,'PENDIENTE'),(69,'7',4,12,0,0,'PENDIENTE'),(70,'7',8,3,0,0,'PENDIENTE');
INSERT INTO `partidos` VALUES (71,'8',9,5,0,0,'PENDIENTE'),(72,'8',1,15,0,0,'PENDIENTE'),(73,'8',18,19,0,0,'PENDIENTE'),(74,'8',2,11,0,0,'PENDIENTE'),(75,'8',17,13,0,0,'PENDIENTE'),(76,'8',10,20,0,0,'PENDIENTE'),(77,'8',7,6,0,0,'PENDIENTE'),(78,'8',14,4,0,0,'PENDIENTE'),(79,'8',3,16,0,0,'PENDIENTE'),(80,'8',12,8,0,0,'PENDIENTE');
INSERT INTO `partidos` VALUES (81,'9',15,5,0,0,'PENDIENTE'),(82,'9',9,18,0,0,'PENDIENTE'),(83,'9',11,1,0,0,'PENDIENTE'),(84,'9',19,17,0,0,'PENDIENTE'),(85,'9',20,2,0,0,'PENDIENTE'),(86,'9',13,7,0,0,'PENDIENTE'),(87,'9',4,10,0,0,'PENDIENTE'),(88,'9',6,3,0,0,'PENDIENTE'),(89,'9',8,14,0,0,'PENDIENTE'),(90,'9',16,12,0,0,'PENDIENTE');
INSERT INTO `partidos` VALUES (91,'10',18,5,0,0,'PENDIENTE'),(92,'10',15,11,0,0,'PENDIENTE'),(93,'10',17,9,0,0,'PENDIENTE'),(94,'10',1,20,0,0,'PENDIENTE'),(95,'10',7,19,0,0,'PENDIENTE'),(96,'10',2,4,0,0,'PENDIENTE'),(97,'10',3,13,0,0,'PENDIENTE'),(98,'10',10,8,0,0,'PENDIENTE'),(99,'10',12,6,0,0,'PENDIENTE'),(100,'10',14,16,0,0,'PENDIENTE');

INSERT INTO `partidos` VALUES (101,'11',11,5,0,0,'PENDIENTE'),(102,'11',18,17,0,0,'PENDIENTE'),(103,'11',20,15,0,0,'PENDIENTE'),(104,'11',9,7,0,0,'PENDIENTE'),(105,'11',4,1,0,0,'PENDIENTE'),(106,'11',19,3,0,0,'PENDIENTE'),(107,'11',13,12,0,0,'PENDIENTE'),(108,'11',16,10,0,0,'PENDIENTE'),(109,'11',6,14,0,0,'PENDIENTE'),(110,'11',8,2,0,0,'PENDIENTE');
INSERT INTO `partidos` VALUES (111,'12',17,5,0,0,'PENDIENTE'),(112,'12',11,20,0,0,'PENDIENTE'),(113,'12',7,18,0,0,'PENDIENTE'),(114,'12',15,4,0,0,'PENDIENTE'),(115,'12',3,9,0,0,'PENDIENTE'),(116,'12',1,8,0,0,'PENDIENTE'),(117,'12',12,19,0,0,'PENDIENTE'),(118,'12',2,16,0,0,'PENDIENTE'),(119,'12',14,13,0,0,'PENDIENTE'),(120,'12',10,6,0,0,'PENDIENTE');
INSERT INTO `partidos` VALUES (121,'13',20,5,0,0,'PENDIENTE'),(122,'13',17,7,0,0,'PENDIENTE'),(123,'13',4,11,0,0,'PENDIENTE'),(124,'13',18,3,0,0,'PENDIENTE'),(125,'13',8,15,0,0,'PENDIENTE'),(126,'13',9,12,0,0,'PENDIENTE'),(127,'13',16,1,0,0,'PENDIENTE'),(128,'13',19,14,0,0,'PENDIENTE'),(129,'13',6,2,0,0,'PENDIENTE'),(130,'13',13,10,0,0,'PENDIENTE');
INSERT INTO `partidos` VALUES (131,'14',7,5,0,0,'PENDIENTE'),(132,'14',20,4,0,0,'PENDIENTE'),(133,'14',3,17,0,0,'PENDIENTE'),(134,'14',11,8,0,0,'PENDIENTE'),(135,'14',12,18,0,0,'PENDIENTE'),(136,'14',15,16,0,0,'PENDIENTE'),(137,'14',14,9,0,0,'PENDIENTE'),(138,'14',1,6,0,0,'PENDIENTE'),(139,'14',10,19,0,0,'PENDIENTE'),(140,'14',2,13,0,0,'PENDIENTE');
INSERT INTO `partidos` VALUES (141,'15',4,5,0,0,'PENDIENTE'),(142,'15',7,3,0,0,'PENDIENTE'),(143,'15',8,20,0,0,'PENDIENTE'),(144,'15',17,12,0,0,'PENDIENTE'),(145,'15',16,11,0,0,'PENDIENTE'),(146,'15',18,14,0,0,'PENDIENTE'),(147,'15',6,15,0,0,'PENDIENTE'),(148,'15',9,10,0,0,'PENDIENTE'),(149,'15',13,1,0,0,'PENDIENTE'),(150,'15',19,2,0,0,'PENDIENTE');
INSERT INTO `partidos` VALUES (151,'16',3,5,0,0,'PENDIENTE'),(152,'16',4,8,0,0,'PENDIENTE'),(153,'16',12,7,0,0,'PENDIENTE'),(154,'16',20,16,0,0,'PENDIENTE'),(155,'16',14,17,0,0,'PENDIENTE'),(156,'16',11,6,0,0,'PENDIENTE'),(157,'16',10,18,0,0,'PENDIENTE'),(158,'16',15,13,0,0,'PENDIENTE'),(159,'16',2,9,0,0,'PENDIENTE'),(160,'16',1,19,0,0,'PENDIENTE');
INSERT INTO `partidos` VALUES (161,'17',8,5,0,0,'PENDIENTE'),(162,'17',3,12,0,0,'PENDIENTE'),(163,'17',16,4,0,0,'PENDIENTE'),(164,'17',7,14,0,0,'PENDIENTE'),(165,'17',6,20,0,0,'PENDIENTE'),(166,'17',17,10,0,0,'PENDIENTE'),(167,'17',13,11,0,0,'PENDIENTE'),(168,'17',18,12,0,0,'PENDIENTE'),(169,'17',19,15,0,0,'PENDIENTE'),(170,'17',9,1,0,0,'PENDIENTE');
INSERT INTO `partidos` VALUES (171,'18',12,5,0,0,'PENDIENTE'),(172,'18',8,16,0,0,'PENDIENTE'),(173,'18',14,3,0,0,'PENDIENTE'),(174,'18',4,6,0,0,'PENDIENTE'),(175,'18',10,7,0,0,'PENDIENTE'),(176,'18',20,13,0,0,'PENDIENTE'),(177,'18',2,17,0,0,'PENDIENTE'),(178,'18',11,19,0,0,'PENDIENTE'),(179,'18',1,18,0,0,'PENDIENTE'),(180,'18',15,9,0,0,'PENDIENTE');
INSERT INTO `partidos` VALUES (181,'19',16,5,0,0,'PENDIENTE'),(182,'19',12,14,0,0,'PENDIENTE'),(183,'19',6,8,0,0,'PENDIENTE'),(184,'19',3,10,0,0,'PENDIENTE'),(185,'19',13,4,0,0,'PENDIENTE'),(186,'19',7,2,0,0,'PENDIENTE'),(187,'19',19,20,0,0,'PENDIENTE'),(188,'19',17,1,0,0,'PENDIENTE'),(189,'19',9,11,0,0,'PENDIENTE'),(190,'19',18,15,0,0,'PENDIENTE');
INSERT INTO `partidos` VALUES (191,'20',5,16,0,0,'PENDIENTE'),(192,'20',14,12,0,0,'PENDIENTE'),(193,'20',8,6,0,0,'PENDIENTE'),(194,'20',10,3,0,0,'PENDIENTE'),(195,'20',4,13,0,0,'PENDIENTE'),(196,'20',2,7,0,0,'PENDIENTE'),(197,'20',20,19,0,0,'PENDIENTE'),(198,'20',1,17,0,0,'PENDIENTE'),(199,'20',11,9,0,0,'PENDIENTE'),(200,'20',15,18,0,0,'PENDIENTE');

INSERT INTO `partidos` VALUES (201,'21',5,12,0,0,'PENDIENTE'),(202,'21',16,8,0,0,'PENDIENTE'),(203,'21',3,14,0,0,'PENDIENTE'),(204,'21',6,4,0,0,'PENDIENTE'),(205,'21',7,10,0,0,'PENDIENTE'),(206,'21',13,20,0,0,'PENDIENTE'),(207,'21',17,2,0,0,'PENDIENTE'),(208,'21',19,11,0,0,'PENDIENTE'),(209,'21',18,1,0,0,'PENDIENTE'),(210,'21',9,15,0,0,'PENDIENTE');
INSERT INTO `partidos` VALUES (211,'22',5,8,0,0,'PENDIENTE'),(212,'22',12,3,0,0,'PENDIENTE'),(213,'22',4,16,0,0,'PENDIENTE'),(214,'22',14,7,0,0,'PENDIENTE'),(215,'22',20,6,0,0,'PENDIENTE'),(216,'22',10,17,0,0,'PENDIENTE'),(217,'22',11,13,0,0,'PENDIENTE'),(218,'22',2,18,0,0,'PENDIENTE'),(219,'22',15,19,0,0,'PENDIENTE'),(220,'22',1,9,0,0,'PENDIENTE');
INSERT INTO `partidos` VALUES (221,'23',5,3,0,0,'PENDIENTE'),(222,'23',8,4,0,0,'PENDIENTE'),(223,'23',7,12,0,0,'PENDIENTE'),(224,'23',16,20,0,0,'PENDIENTE'),(225,'23',17,14,0,0,'PENDIENTE'),(226,'23',6,11,0,0,'PENDIENTE'),(227,'23',18,10,0,0,'PENDIENTE'),(228,'23',13,15,0,0,'PENDIENTE'),(229,'23',9,2,0,0,'PENDIENTE'),(230,'23',19,1,0,0,'PENDIENTE');
INSERT INTO `partidos` VALUES (231,'24',5,4,0,0,'PENDIENTE'),(232,'24',3,7,0,0,'PENDIENTE'),(233,'24',20,8,0,0,'PENDIENTE'),(234,'24',12,17,0,0,'PENDIENTE'),(235,'24',11,16,0,0,'PENDIENTE'),(236,'24',14,18,0,0,'PENDIENTE'),(237,'24',15,6,0,0,'PENDIENTE'),(238,'24',10,9,0,0,'PENDIENTE'),(239,'24',1,13,0,0,'PENDIENTE'),(240,'24',2,19,0,0,'PENDIENTE');
INSERT INTO `partidos` VALUES (241,'25',5,7,0,0,'PENDIENTE'),(242,'25',4,20,0,0,'PENDIENTE'),(243,'25',17,3,0,0,'PENDIENTE'),(244,'25',8,11,0,0,'PENDIENTE'),(245,'25',18,12,0,0,'PENDIENTE'),(246,'25',16,15,0,0,'PENDIENTE'),(247,'25',9,14,0,0,'PENDIENTE'),(248,'25',6,1,0,0,'PENDIENTE'),(249,'25',19,10,0,0,'PENDIENTE'),(250,'25',13,2,0,0,'PENDIENTE');
INSERT INTO `partidos` VALUES (251,'26',5,20,0,0,'PENDIENTE'),(252,'26',7,17,0,0,'PENDIENTE'),(253,'26',11,4,0,0,'PENDIENTE'),(254,'26',3,18,0,0,'PENDIENTE'),(255,'26',15,8,0,0,'PENDIENTE'),(256,'26',12,9,0,0,'PENDIENTE'),(257,'26',1,16,0,0,'PENDIENTE'),(258,'26',14,19,0,0,'PENDIENTE'),(259,'26',2,6,0,0,'PENDIENTE'),(260,'26',10,13,0,0,'PENDIENTE');
INSERT INTO `partidos` VALUES (261,'27',5,17,0,0,'PENDIENTE'),(262,'27',20,11,0,0,'PENDIENTE'),(263,'27',18,7,0,0,'PENDIENTE'),(264,'27',4,15,0,0,'PENDIENTE'),(265,'27',9,13,0,0,'PENDIENTE'),(266,'27',8,1,0,0,'PENDIENTE'),(267,'27',19,12,0,0,'PENDIENTE'),(268,'27',16,2,0,0,'PENDIENTE'),(269,'27',13,14,0,0,'PENDIENTE'),(270,'27',6,10,0,0,'PENDIENTE');
INSERT INTO `partidos` VALUES (271,'28',5,11,0,0,'PENDIENTE'),(272,'28',17,18,0,0,'PENDIENTE'),(273,'28',15,20,0,0,'PENDIENTE'),(274,'28',7,9,0,0,'PENDIENTE'),(275,'28',1,4,0,0,'PENDIENTE'),(276,'28',3,19,0,0,'PENDIENTE'),(277,'28',2,8,0,0,'PENDIENTE'),(278,'28',12,13,0,0,'PENDIENTE'),(279,'28',10,16,0,0,'PENDIENTE'),(280,'28',14,6,0,0,'PENDIENTE');
INSERT INTO `partidos` VALUES (281,'29',5,18,0,0,'PENDIENTE'),(282,'29',11,15,0,0,'PENDIENTE'),(283,'29',9,17,0,0,'PENDIENTE'),(284,'29',20,1,0,0,'PENDIENTE'),(285,'29',19,7,0,0,'PENDIENTE'),(286,'29',4,2,0,0,'PENDIENTE'),(287,'29',13,3,0,0,'PENDIENTE'),(288,'29',8,10,0,0,'PENDIENTE'),(289,'29',6,12,0,0,'PENDIENTE'),(290,'29',16,14,0,0,'PENDIENTE');
INSERT INTO `partidos` VALUES (291,'30',5,15,0,0,'PENDIENTE'),(292,'30',18,9,0,0,'PENDIENTE'),(293,'30',1,11,0,0,'PENDIENTE'),(294,'30',17,19,0,0,'PENDIENTE'),(295,'30',2,20,0,0,'PENDIENTE'),(296,'30',7,13,0,0,'PENDIENTE'),(297,'30',10,4,0,0,'PENDIENTE'),(298,'30',3,6,0,0,'PENDIENTE'),(299,'30',14,8,0,0,'PENDIENTE'),(300,'30',12,16,0,0,'PENDIENTE');

INSERT INTO `partidos` VALUES (301,'31',5,9,0,0,'PENDIENTE'),(302,'31',15,1,0,0,'PENDIENTE'),(303,'31',19,18,0,0,'PENDIENTE'),(304,'31',11,2,0,0,'PENDIENTE'),(305,'31',13,17,0,0,'PENDIENTE'),(306,'31',20,10,0,0,'PENDIENTE'),(307,'31',6,7,0,0,'PENDIENTE'),(308,'31',4,14,0,0,'PENDIENTE'),(309,'31',16,3,0,0,'PENDIENTE'),(310,'31',8,12,0,0,'PENDIENTE');
INSERT INTO `partidos` VALUES (311,'32',5,1,0,0,'PENDIENTE'),(312,'32',9,19,0,0,'PENDIENTE'),(313,'32',2,15,0,0,'PENDIENTE'),(314,'32',18,13,0,0,'PENDIENTE'),(315,'32',10,11,0,0,'PENDIENTE'),(316,'32',17,6,0,0,'PENDIENTE'),(317,'32',14,20,0,0,'PENDIENTE'),(318,'32',7,16,0,0,'PENDIENTE'),(319,'32',12,4,0,0,'PENDIENTE'),(320,'32',3,8,0,0,'PENDIENTE');
INSERT INTO `partidos` VALUES (321,'33',5,19,0,0,'PENDIENTE'),(322,'33',1,2,0,0,'PENDIENTE'),(323,'33',13,9,0,0,'PENDIENTE'),(324,'33',15,10,0,0,'PENDIENTE'),(325,'33',6,18,0,0,'PENDIENTE'),(326,'33',11,14,0,0,'PENDIENTE'),(327,'33',16,17,0,0,'PENDIENTE'),(328,'33',20,12,0,0,'PENDIENTE'),(329,'33',8,7,0,0,'PENDIENTE'),(330,'33',4,3,0,0,'PENDIENTE');
INSERT INTO `partidos` VALUES (331,'34',5,2,0,0,'PENDIENTE'),(332,'34',19,13,0,0,'PENDIENTE'),(333,'34',10,1,0,0,'PENDIENTE'),(334,'34',9,6,0,0,'PENDIENTE'),(335,'34',14,15,0,0,'PENDIENTE'),(336,'34',18,16,0,0,'PENDIENTE'),(337,'34',12,11,0,0,'PENDIENTE'),(338,'34',17,8,0,0,'PENDIENTE'),(339,'34',3,20,0,0,'PENDIENTE'),(340,'34',7,4,0,0,'PENDIENTE');
INSERT INTO `partidos` VALUES (341,'35',5,13,0,0,'PENDIENTE'),(342,'35',2,10,0,0,'PENDIENTE'),(343,'35',6,19,0,0,'PENDIENTE'),(344,'35',1,14,0,0,'PENDIENTE'),(345,'35',16,9,0,0,'PENDIENTE'),(346,'35',15,12,0,0,'PENDIENTE'),(347,'35',8,18,0,0,'PENDIENTE'),(348,'35',11,3,0,0,'PENDIENTE'),(349,'35',4,17,0,0,'PENDIENTE'),(350,'35',20,7,0,0,'PENDIENTE');
INSERT INTO `partidos` VALUES (351,'36',5,10,0,0,'PENDIENTE'),(352,'36',13,6,0,0,'PENDIENTE'),(353,'36',14,2,0,0,'PENDIENTE'),(354,'36',19,16,0,0,'PENDIENTE'),(355,'36',12,1,0,0,'PENDIENTE'),(356,'36',9,8,0,0,'PENDIENTE'),(357,'36',3,15,0,0,'PENDIENTE'),(358,'36',18,4,0,0,'PENDIENTE'),(359,'36',7,11,0,0,'PENDIENTE'),(360,'36',17,20,0,0,'PENDIENTE');
INSERT INTO `partidos` VALUES (361,'37',5,6,0,0,'PENDIENTE'),(362,'37',10,14,0,0,'PENDIENTE'),(363,'37',16,13,0,0,'PENDIENTE'),(364,'37',2,12,0,0,'PENDIENTE'),(365,'37',8,19,0,0,'PENDIENTE'),(366,'37',1,3,0,0,'PENDIENTE'),(367,'37',4,9,0,0,'PENDIENTE'),(368,'37',15,7,0,0,'PENDIENTE'),(369,'37',20,18,0,0,'PENDIENTE'),(370,'37',11,17,0,0,'PENDIENTE');
INSERT INTO `partidos` VALUES (371,'38',5,14,0,0,'PENDIENTE'),(372,'38',6,16,0,0,'PENDIENTE'),(373,'38',12,10,0,0,'PENDIENTE'),(374,'38',13,8,0,0,'PENDIENTE'),(375,'38',3,2,0,0,'PENDIENTE'),(376,'38',19,4,0,0,'PENDIENTE'),(377,'38',7,1,0,0,'PENDIENTE'),(378,'38',9,20,0,0,'PENDIENTE'),(379,'38',17,15,0,0,'PENDIENTE'),(380,'38',18,11,0,0,'PENDIENTE');


/*!40000 ALTER TABLE `partidos` ENABLE KEYS */;
UNLOCK TABLES;

LOCK TABLES `tipos_lance` WRITE;
/*!40000 ALTER TABLE `tipos_lance` DISABLE KEYS */;
INSERT INTO `tipos_lance` VALUES ('AGRESIÓN'),('CORNER'),('FALTA'),('FALTA PELIGROSA'),('GOL'),('INCIDENCIA'),('JUGADA'),('LESIÓN'),('OCASIÓN'),('PENALTY'),('TARJETA AMARILLA');
/*!40000 ALTER TABLE `tipos_lance` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;


