<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Indice</title>
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/css/bootstrap.min.css"
	integrity="sha384-rwoIResjU2yc3z8GV/NPeZWAv56rSmLldC3R/AZzGRnGxQQKnKkoFVhFQhNUwEyJ"
	crossorigin="anonymous">
<link rel="stylesheet" type="text/css" href="/css/styles.css">
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">

</head>
<body>
	<nav class="navbar navbar-toggleable-md navbar-light bg-faded">
		<button class="navbar-toggler navbar-toggler-right" type="button"
			data-toggle="collapse" data-target="#navbarNav"
			aria-controls="navbarNav" aria-expanded="false"
			aria-label="Toggle navigation">
			<span class="navbar-toggler-icon"></span>
		</button>
		<a class="navbar-brand" href="/diariodeportivo/app/"><i
			class="far fa-futbol"></i>Goles con Salsa</a>
		<div class="collapse navbar-collapse" id="navbarNav">
			<ul class="navbar-nav">
				<li class="nav-item active"><a class="nav-link"
					href="/diariodeportivo/app/partidos/jornada?id=1">Jornadas<span
						class="sr-only">(current)</span></a></li>
				<li class="nav-item"><a class="nav-link"
					href="/diariodeportivo/app/partidos">Partidos</a></li>
				<li class="nav-item"><a class="nav-link"
					href="/diariodeportivo/app/clasificacion">Clasificacion</a></li>
				<li class="nav-item"><a class="nav-link my-2 my-lg-0"
					href="/swagger-ui.html" align="right" target="_blank">Documentación
						API</a></li>

			</ul>
		</div>
	</nav>
</body>
</html>