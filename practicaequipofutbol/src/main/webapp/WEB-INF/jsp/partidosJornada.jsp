<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>DEMO JSP</title>
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/css/bootstrap.min.css"
	integrity="sha384-rwoIResjU2yc3z8GV/NPeZWAv56rSmLldC3R/AZzGRnGxQQKnKkoFVhFQhNUwEyJ"
	crossorigin="anonymous">
<link rel="stylesheet" type="text/css" href="/css/styles.css">
</head>
<body>
<jsp:include page="cabecera.jsp"></jsp:include>
	<ul class="nav justify-content-center">
		<c:forEach begin="1" end="${numJornadas}" varStatus="loop">
			<li class="nav-item"><a class="nav-link active" href="/diariodeportivo/app/partidos/jornada?id=${loop.index}"><div
						class="card">
						<div class="card-block">Jornada ${loop.index}</div>
					</div></a></li>

		</c:forEach>
	</ul>

	<h2 class="text-center">Jornada</h2>
	<br>
	<h1 class="text-center">Partidos</h1>

	<div class="container">
		<c:forEach var="partido" items="${partidosJornada}">
			<div class="row">
				<div class="col-sm-6">
					<div class="card mx-auto mt-4 pt-4 pb-4" style="width: 30rem;">

						<div class="row">
							<div class="col-sm-3 mt-2">
								<img class="logoLocal" src="/img/logoBarcelona.png">
								<p class="text-center ml-4 mt-2">
									<strong>${partido.local.nombre} </strong>
								</p>

							</div>
							<div class="col-sm-6">
								<div class="horario">
									<p class="goles">
										<strong>${partido.golesLocal}</strong>
									</p>
									<p class="estado">${partido.estado}</p>
									<p class="goles">
										<strong>${partido.golesVisitante}</strong>
									</p>
								</div>
								<a href="/diariodeportivo/app/lances/partido?id=${partido.id}" class="btn btn-primary botonVer">Ver
									Partido</a>
							</div>
							<div class="col-sm-3 mt-2">
								<img class="logoVisitante" src="/img/logoMadrid.jpg">
								<p class="text-center mr-2 mt-2">
									<strong>${partido.visitante.nombre}</strong>
								</p>
							</div>
						</div>
					</div>
				</div>
		</c:forEach>
		
	</div>
	</div>
</body>
</html>