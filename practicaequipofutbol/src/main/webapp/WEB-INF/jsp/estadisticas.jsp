<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>DEMO JSP</title>
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/css/bootstrap.min.css"
	integrity="sha384-rwoIResjU2yc3z8GV/NPeZWAv56rSmLldC3R/AZzGRnGxQQKnKkoFVhFQhNUwEyJ"
	crossorigin="anonymous">
<link rel="stylesheet" type="text/css" href="/css/styles.css">
</head>
<body>
	<jsp:include page="cabecera.jsp"></jsp:include>
	<br>
	<div class="container">
		<div class="row">
			<div class="col-sm-2">
			<img class="mx-auto d-block mt-4" src="/img/logoBarcelona.png">
			<p class="text-center mt-2"><strong>${estadistica.partido.local.nombre}</strong></p>
			</div>
			
			<div class="col-sm-8">
			<h1 class="text-center tituloStats">Estadísticas del partido</h1>
			</div>
			
			<div class="col-sm-2">
			<img class="mx-auto d-block mt-4" src="/img/logoMadrid.jpg">
			<p class="text-center mt-2"><strong>${estadistica.partido.visitante.nombre}</strong></p>
			</div>
			
		</div>
	
	
	
	
	
	</div>

	<div class="container">
		<p class="text-center mt-4 mb-4">Agresiones</p>
		<div class="row">
			<div class="col-sm-2">
				<p class="text-center">${estadistica.numAgresionesLocal}</p>
			</div>

			<div class="col-sm-8">
				<div class="progress">
					<div class="progress-bar" role="progressbar" style="width: ${estadistica.agresionesPorcentajeLocal}%"
						aria-valuenow="15" aria-valuemin="0" aria-valuemax="100"></div>
					<div class="progress-bar bg-danger" role="progressbar"
						style="width: ${estadistica.agresionesPorcentajeVisitante}%" aria-valuenow="30" aria-valuemin="0"
						aria-valuemax="100"></div>
				</div>
			</div>
			<div class="col-sm-2">
				<p class="text-center">${estadistica.numAgresionesVisitante}</p>
			</div>
		</div>
				<p class="text-center mt-4 mb-4">Corner</p>
		<div class="row">
			<div class="col-sm-2">
				<p class="text-center">${estadistica.numCornersLocal}</p>
			</div>

			<div class="col-sm-8">
				<div class="progress">
					<div class="progress-bar" role="progressbar" style="width: ${estadistica.cornersPorcentajeLocal}%"
						aria-valuenow="15" aria-valuemin="0" aria-valuemax="100"></div>
					<div class="progress-bar bg-danger" role="progressbar"
						style="width: ${estadistica.cornersPorcentajeVisitante}%" aria-valuenow="30" aria-valuemin="0"
						aria-valuemax="100"></div>
				</div>
			</div>
			<div class="col-sm-2">
				<p class="text-center">${estadistica.numCornersVisitante}</p>
			</div>
		</div>
				<p class="text-center mt-4 mb-4">Falta</p>
		<div class="row">
			<div class="col-sm-2">
				<p class="text-center">${estadistica.numFaltasLocal}</p>
			</div>

			<div class="col-sm-8">
				<div class="progress">
					<div class="progress-bar" role="progressbar" style="width: ${estadistica.faltasPorcentajeLocal}%"
						aria-valuenow="15" aria-valuemin="0" aria-valuemax="100"></div>
					<div class="progress-bar bg-danger" role="progressbar"
						style="width: ${estadistica.faltasPorcentajeVisitante}%" aria-valuenow="30" aria-valuemin="0"
						aria-valuemax="100"></div>
				</div>
			</div>
			<div class="col-sm-2">
				<p class="text-center">${estadistica.numFaltasVisitante}</p>
			</div>
		</div>
				<p class="text-center mt-4 mb-4">Falta Peligrosa</p>
		<div class="row">
			<div class="col-sm-2">
				<p class="text-center">${estadistica.numFaltasPeligrosasLocal}</p>
			</div>

			<div class="col-sm-8">
				<div class="progress">
					<div class="progress-bar" role="progressbar" style="width: ${estadistica.faltasPeligrosasPorcentajeLocal}%"
						aria-valuenow="15" aria-valuemin="0" aria-valuemax="100"></div>
					<div class="progress-bar bg-danger" role="progressbar"
						style="width: ${estadistica.faltasPeligrosasPorcentajeVisitante}%" aria-valuenow="30" aria-valuemin="0"
						aria-valuemax="100"></div>
				</div>
			</div>
			<div class="col-sm-2">
				<p class="text-center">${estadistica.numFaltasPeligrosasVisitante}</p>
			</div>
		</div>
				<p class="text-center mt-4 mb-4">Gol</p>
		<div class="row">
			<div class="col-sm-2">
				<p class="text-center">${estadistica.numGolesLocal}</p>
			</div>

			<div class="col-sm-8">
				<div class="progress">
					<div class="progress-bar" role="progressbar" style="width: ${estadistica.golesPorcentajeLocal}%"
						aria-valuenow="15" aria-valuemin="0" aria-valuemax="100"></div>
					<div class="progress-bar bg-danger" role="progressbar"
						style="width: ${estadistica.golesPorcentajeVisitante}%" aria-valuenow="30" aria-valuemin="0"
						aria-valuemax="100"></div>
				</div>
			</div>
			<div class="col-sm-2">
				<p class="text-center">${estadistica.numGolesVisitante}</p>
			</div>
		</div>
				<p class="text-center mt-4 mb-4">Incidencia</p>
		<div class="row">
			<div class="col-sm-2">
				<p class="text-center">${estadistica.numIncidenciasLocal}</p>
			</div>

			<div class="col-sm-8">
				<div class="progress">
					<div class="progress-bar" role="progressbar" style="width: ${estadistica.incidenciasPorcentajeLocal}%"
						aria-valuenow="15" aria-valuemin="0" aria-valuemax="100"></div>
					<div class="progress-bar bg-danger" role="progressbar"
						style="width: ${estadistica.incidenciasPorcentajeVisitante}%" aria-valuenow="30" aria-valuemin="0"
						aria-valuemax="100"></div>
				</div>
			</div>
			<div class="col-sm-2">
				<p class="text-center">${estadistica.numIncidenciasVisitante}</p>
			</div>
		</div>
				<p class="text-center mt-4 mb-4">Jugada</p>
		<div class="row">
			<div class="col-sm-2">
				<p class="text-center">${estadistica.numJugadasLocal}</p>
			</div>

			<div class="col-sm-8">
				<div class="progress">
					<div class="progress-bar" role="progressbar" style="width: ${estadistica.jugadasPorcentajeLocal}%"
						aria-valuenow="15" aria-valuemin="0" aria-valuemax="100"></div>
					<div class="progress-bar bg-danger" role="progressbar"
						style="width: ${estadistica.jugadasPorcentajeVisitante}%" aria-valuenow="30" aria-valuemin="0"
						aria-valuemax="100"></div>
				</div>
			</div>
			<div class="col-sm-2">
				<p class="text-center">${estadistica.numJugadasVisitante}</p>
			</div>
		</div>
				<p class="text-center mt-4 mb-4">Lesión</p>
		<div class="row">
			<div class="col-sm-2">
				<p class="text-center">${estadistica.numLesionesLocal}</p>
			</div>

			<div class="col-sm-8">
				<div class="progress">
					<div class="progress-bar" role="progressbar" style="width: ${estadistica.lesionesPorcentajeLocal}%"
						aria-valuenow="15" aria-valuemin="0" aria-valuemax="100"></div>
					<div class="progress-bar bg-danger" role="progressbar"
						style="width: ${estadistica.lesionesPorcentajeVisitante}%" aria-valuenow="30" aria-valuemin="0"
						aria-valuemax="100"></div>
				</div>
			</div>
			<div class="col-sm-2">
				<p class="text-center">${estadistica.numLesionesVisitante}</p>
			</div>
		</div>
				<p class="text-center mt-4 mb-4">Ocasión</p>
		<div class="row">
			<div class="col-sm-2">
				<p class="text-center">${estadistica.numOcasionGolLocal}</p>
			</div>

			<div class="col-sm-8">
				<div class="progress">
					<div class="progress-bar" role="progressbar" style="width: ${estadistica.ocasionGolPorcentajeLocal}%"
						aria-valuenow="15" aria-valuemin="0" aria-valuemax="100"></div>
					<div class="progress-bar bg-danger" role="progressbar"
						style="width: ${estadistica.ocasionGolPorcentajeVisitante}%" aria-valuenow="30" aria-valuemin="0"
						aria-valuemax="100"></div>
				</div>
			</div>
			<div class="col-sm-2">
				<p class="text-center">${estadistica.numOcasionGolVisitante}</p>
			</div>
		</div>
				<p class="text-center mt-4 mb-4">Penalty</p>
		<div class="row">
			<div class="col-sm-2">
				<p class="text-center">${estadistica.numPenaltiesLocal}</p>
			</div>

			<div class="col-sm-8">
				<div class="progress">
					<div class="progress-bar" role="progressbar" style="width: ${estadistica.penaltiesPorcentajeLocal}%"
						aria-valuenow="15" aria-valuemin="0" aria-valuemax="100"></div>
					<div class="progress-bar bg-danger" role="progressbar"
						style="width: ${estadistica.penaltiesPorcentajeVisitante}%" aria-valuenow="30" aria-valuemin="0"
						aria-valuemax="100"></div>
				</div>
			</div>
			<div class="col-sm-2">
				<p class="text-center">${estadistica.numPenaltiesVisitante}</p>
			</div>
		</div>
				<p class="text-center mt-4 mb-4">Tarjeta Amarilla</p>
		<div class="row">
			<div class="col-sm-2">
				<p class="text-center">${estadistica.numTarjetaAmarillaLocal}</p>
			</div>

			<div class="col-sm-8">
				<div class="progress">
					<div class="progress-bar" role="progressbar" style="width: ${estadistica.tarjetaAmarillaPorcentajeLocal}%"
						aria-valuenow="15" aria-valuemin="0" aria-valuemax="100"></div>
					<div class="progress-bar bg-danger" role="progressbar"
						style="width: ${estadistica.tarjetaAmarillaPorcentajeVisitante}%" aria-valuenow="30" aria-valuemin="0"
						aria-valuemax="100"></div>
				</div>
			</div>
			<div class="col-sm-2">
				<p class="text-center" >${estadistica.numTarjetaAmarillaVisitante}</p>
			</div>
			
		</div>
		<p class="text-center mt-4 mb-4">Tarjeta Roja</p>
		<div class="row">
			<div class="col-sm-2">
				<p class="text-center">${estadistica.numTarjetaRojaLocal}</p>
			</div>

			<div class="col-sm-8">
				<div class="progress">
					<div class="progress-bar" role="progressbar" style="width: ${estadistica.tarjetaRojaPorcentajeLocal}%"
						aria-valuenow="15" aria-valuemin="0" aria-valuemax="100"></div>
					<div class="progress-bar bg-danger" role="progressbar"
						style="width: ${estadistica.tarjetaRojaPorcentajeVisitante}%" aria-valuenow="30" aria-valuemin="0"
						aria-valuemax="100"></div>
				</div>
			</div>
			<div class="col-sm-2">
				<p class="text-center" >${estadistica.numTarjetaRojaVisitante}</p>
			</div>
			
		</div>
	


		

	</div>
</body>
</html>