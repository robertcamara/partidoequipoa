<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/css/bootstrap.min.css"
	integrity="sha384-rwoIResjU2yc3z8GV/NPeZWAv56rSmLldC3R/AZzGRnGxQQKnKkoFVhFQhNUwEyJ"
	crossorigin="anonymous">
<link rel="stylesheet" type="text/css" href="/css/styles.css">
</head>
<body>
   <jsp:include page="cabecera.jsp"></jsp:include>
	<div class="container">
		<table class="table table-striped">
			<thead>
				<tr>
					<th>Partido</th>
					<th>Jornada</th>
					<th>Equipo Local</th>
					<th>Equipo Visitante</th>
					<th>Goles Local</th>
					<th>Goles Visitante</th>
					<th>Estado</th>
					<th>Cambiar estado</th>
					<th>Ficha</th>
					<th>Estadísticas</th>
				</tr>
			</thead>
			<tbody>
				<c:forEach var="partido" items="${listadoPartidos}">
					<tr>

						<th scope="row">${partido.id}</th>
						<td>${partido.jornada}</td>
						<td>${partido.local.nombre}</td>
						<td>${partido.visitante.nombre}</td>
						<td>${partido.golesLocal}</td>
						<td>${partido.golesVisitante}</td>
						<td>${partido.estado}</td>
						<td><a href="partidos/estado?id=${partido.id}" >Cambiar estado</a></td>
						<c:if test="${partido.estado == 'PENDIENTE' }">
							<td>
							<p>botonficha</p>
													
							</td>
							<td>
							<p>Estadísticas</p>
													
							</td>
						</c:if>
						<c:if test="${partido.estado != 'PENDIENTE' }">
							<td>
							<a href="lances/partido?id=${partido.id}">Ficha</a>
							</td>
							<td>
							<a href="estadisticas/partido?id=${partido.id}">Estadísticas</a>
							</td>
						</c:if>
			
					</tr>
				</c:forEach>
				
			</tbody>
		</table>
	</div>
</body>
</html>