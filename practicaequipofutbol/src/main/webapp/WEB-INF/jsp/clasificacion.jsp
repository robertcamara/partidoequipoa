<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/css/bootstrap.min.css"
	integrity="sha384-rwoIResjU2yc3z8GV/NPeZWAv56rSmLldC3R/AZzGRnGxQQKnKkoFVhFQhNUwEyJ"
	crossorigin="anonymous">
<link rel="stylesheet" type="text/css" href="/css/styles.css">
</head>
<body>
	<jsp:include page="cabecera.jsp"></jsp:include>
	<div class="container">
		<table class="table table-striped">
			<thead>
				<tr>
					<th>Equipo</th>
					<th>Puntos</th>
					<th>PG</th>
					<th>PE</th>
					<th>PP</th>
					<th>GF</th>
					<th>GC</th>
				
				</tr>
			</thead>
			<tbody>
			<c:forEach var="equipo" items="${clasificacion}">
				<tr>
					<th scope="row">${equipo.equipo.nombre}</th>
					<td>${equipo.puntosTotal}</td>
					<td>${equipo.PGL + equipo.PGV}</td>
					<td>${equipo.PEL + equipo.PEV}</td>
					<td>${equipo.PPL + equipo.PPV}</td>
					<td>${equipo.GFL + equipo.GFV}</td>
					<td>${equipo.GCL + equipo.GCV}</td> 
														
				</tr>
				</c:forEach>
				
			</tbody>
		</table>
	</div>
</body>
</html>